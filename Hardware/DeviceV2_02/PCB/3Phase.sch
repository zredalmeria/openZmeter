EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 3Phase:MCP6004-Components U1
U 3 1 5BD043B1
P 5975 4175
F 0 "U1" H 6025 4325 50  0000 L CNN
F 1 "MCP6004" H 5875 3975 50  0000 L CNN
F 2 "Footprints:SOIC-14_3.9x8.7mm_P1.27mm" H 5925 4275 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/74-Series_NXP_74HC4066PW_74HC4066PW_C5650.html" H 6025 4375 50  0001 C CNN
	3    5975 4175
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:MCP6004-Components U1
U 2 1 5BD0441A
P 4975 4075
F 0 "U1" H 4975 4275 50  0000 L CNN
F 1 "MCP6004" H 4800 3825 50  0000 L CNN
F 2 "Footprints:SOIC-14_3.9x8.7mm_P1.27mm" H 4925 4175 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/74-Series_NXP_74HC4066PW_74HC4066PW_C5650.html" H 5025 4275 50  0001 C CNN
	2    4975 4075
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:MCP6004-Components U1
U 4 1 5BD04495
P 5975 5425
F 0 "U1" H 5975 5625 50  0000 L CNN
F 1 "MCP6004" H 5825 5200 50  0000 L CNN
F 2 "Footprints:SOIC-14_3.9x8.7mm_P1.27mm" H 5925 5525 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/74-Series_NXP_74HC4066PW_74HC4066PW_C5650.html" H 6025 5625 50  0001 C CNN
	4    5975 5425
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R8
U 1 1 5BD04895
P 4475 4075
F 0 "R8" V 4555 4075 50  0000 C CNN
F 1 "100h" V 4475 4075 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 4405 4075 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_100R-100R-1_C125923.html" H 4475 4075 50  0001 C CNN
	1    4475 4075
	0    -1   1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R11
U 1 1 5BD0490E
P 6125 6875
F 0 "R11" V 6205 6875 50  0000 C CNN
F 1 "1k02" V 6125 6875 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6055 6875 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Others_YAGEO-AC0603FR-071K02L_C227642.html" H 6125 6875 50  0001 C CNN
	1    6125 6875
	0    -1   1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R14
U 1 1 5BD04949
P 4450 7750
F 0 "R14" V 4530 7750 50  0000 C CNN
F 1 "100h" V 4450 7750 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 4380 7750 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_100R-100R-1_C125923.html" H 4450 7750 50  0001 C CNN
	1    4450 7750
	0    -1   1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R15
U 1 1 5BD04974
P 5125 8300
F 0 "R15" V 5050 8300 50  0000 C CNN
F 1 "1k69" V 5125 8300 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5055 8300 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Others_YAGEO-AC0603FR-071K69L_C227658.html" H 5125 8300 50  0001 C CNN
	1    5125 8300
	0    -1   1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R10
U 1 1 5BD049A1
P 5150 7025
F 0 "R10" V 5230 7025 50  0000 C CNN
F 1 "1k69" V 5150 7025 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5080 7025 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Others_YAGEO-AC0603FR-071K69L_C227658.html" H 5150 7025 50  0001 C CNN
	1    5150 7025
	0    1    -1   0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R7
U 1 1 5BD049D0
P 4475 6475
F 0 "R7" V 4555 6475 50  0000 C CNN
F 1 "100h" V 4475 6475 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 4405 6475 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_100R-100R-1_C125923.html" H 4475 6475 50  0001 C CNN
	1    4475 6475
	0    1    -1   0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R13
U 1 1 5BD04A47
P 5500 6575
F 0 "R13" V 5580 6575 50  0000 C CNN
F 1 "1k02" V 5500 6575 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5430 6575 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Others_YAGEO-AC0603FR-071K02L_C227642.html" H 5500 6575 50  0001 C CNN
	1    5500 6575
	0    1    -1   0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R17
U 1 1 5BD04A80
P 6475 7025
F 0 "R17" V 6400 7025 50  0000 C CNN
F 1 "1k69" V 6475 7025 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6405 7025 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Others_YAGEO-AC0603FR-071K69L_C227658.html" H 6475 7025 50  0001 C CNN
	1    6475 7025
	0    -1   1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R16
U 1 1 5BD04AB5
P 6475 5875
F 0 "R16" V 6555 5875 50  0000 C CNN
F 1 "1k69" V 6475 5875 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6405 5875 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Others_YAGEO-AC0603FR-071K69L_C227658.html" H 6475 5875 50  0001 C CNN
	1    6475 5875
	0    -1   1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R12
U 1 1 5BD04AEC
P 4975 5875
F 0 "R12" V 5055 5875 50  0000 C CNN
F 1 "1k69" V 4975 5875 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 4905 5875 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Others_YAGEO-AC0603FR-071K69L_C227658.html" H 4975 5875 50  0001 C CNN
	1    4975 5875
	0    -1   1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R9
U 1 1 5BD04B25
P 5475 5425
F 0 "R9" V 5555 5425 50  0000 C CNN
F 1 "1k02" V 5475 5425 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5405 5425 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Others_YAGEO-AC0603FR-071K02L_C227642.html" H 5475 5425 50  0001 C CNN
	1    5475 5425
	0    1    -1   0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R6
U 1 1 5BD04B60
P 6125 5725
F 0 "R6" V 6205 5725 50  0000 C CNN
F 1 "1k02" V 6125 5725 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6055 5725 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Others_YAGEO-AC0603FR-071K02L_C227642.html" H 6125 5725 50  0001 C CNN
	1    6125 5725
	0    -1   1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R3
U 1 1 5BD04C05
P 4475 5325
F 0 "R3" V 4555 5325 50  0000 C CNN
F 1 "100h" V 4475 5325 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 4405 5325 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_100R-100R-1_C125923.html" H 4475 5325 50  0001 C CNN
	1    4475 5325
	0    -1   1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R5
U 1 1 5BD04C4A
P 6475 4625
F 0 "R5" V 6555 4625 50  0000 C CNN
F 1 "1k69" V 6475 4625 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6405 4625 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Others_YAGEO-AC0603FR-071K69L_C227658.html" H 6475 4625 50  0001 C CNN
	1    6475 4625
	0    -1   1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R4
U 1 1 5BD04C8B
P 5125 4625
F 0 "R4" V 5205 4625 50  0000 C CNN
F 1 "1k69" H 5125 4625 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5055 4625 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Others_YAGEO-AC0603FR-071K69L_C227658.html" H 5125 4625 50  0001 C CNN
	1    5125 4625
	0    -1   1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R1
U 1 1 5BD04CCE
P 5475 4175
F 0 "R1" V 5555 4175 50  0000 C CNN
F 1 "1k02" V 5475 4175 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5405 4175 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Others_YAGEO-AC0603FR-071K02L_C227642.html" H 5475 4175 50  0001 C CNN
	1    5475 4175
	0    1    -1   0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R2
U 1 1 5BD04D27
P 6125 4475
F 0 "R2" V 6205 4475 50  0000 C CNN
F 1 "1k02" V 6125 4475 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6055 4475 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Others_YAGEO-AC0603FR-071K02L_C227642.html" H 6125 4475 50  0001 C CNN
	1    6125 4475
	0    -1   1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R19
U 1 1 5BD04D6E
P 6125 8150
F 0 "R19" V 6205 8150 50  0000 C CNN
F 1 "1k02" V 6125 8150 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6055 8150 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Others_YAGEO-AC0603FR-071K02L_C227642.html" H 6125 8150 50  0001 C CNN
	1    6125 8150
	0    -1   1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R22
U 1 1 5BD04DB7
P 6475 8300
F 0 "R22" V 6555 8300 50  0000 C CNN
F 1 "1k69" V 6475 8300 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6405 8300 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Others_YAGEO-AC0603FR-071K69L_C227658.html" H 6475 8300 50  0001 C CNN
	1    6475 8300
	0    -1   1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R24
U 1 1 5BD04E02
P 1025 9350
F 0 "R24" V 1105 9350 50  0000 C CNN
F 1 "200k" V 1025 9350 50  0001 C CNN
F 2 "Footprints:R_1206_3216Metric" V 955 9350 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_AC1206FR-07200KL_C144506.html" H 1025 9350 50  0001 C CNN
	1    1025 9350
	0    1    1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R29
U 1 1 5BD04E4F
P 1725 9350
F 0 "R29" V 1805 9350 50  0000 C CNN
F 1 "200k" V 1725 9350 50  0001 C CNN
F 2 "Footprints:R_1206_3216Metric" V 1655 9350 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_AC1206FR-07200KL_C144506.html" H 1725 9350 50  0001 C CNN
	1    1725 9350
	0    1    1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R18
U 1 1 5BD04E9E
P 1025 10000
F 0 "R18" V 1105 10000 50  0000 C CNN
F 1 "200k" V 1025 10000 50  0001 C CNN
F 2 "Footprints:R_1206_3216Metric" V 955 10000 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_AC1206FR-07200KL_C144506.html" H 1025 10000 50  0001 C CNN
	1    1025 10000
	0    1    1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R21
U 1 1 5BD04EEF
P 1025 10650
F 0 "R21" V 1105 10650 50  0000 C CNN
F 1 "200k" V 1025 10650 50  0001 C CNN
F 2 "Footprints:R_1206_3216Metric" V 955 10650 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_AC1206FR-07200KL_C144506.html" H 1025 10650 50  0001 C CNN
	1    1025 10650
	0    1    1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R25
U 1 1 5BD04F42
P 1375 9350
F 0 "R25" V 1455 9350 50  0000 C CNN
F 1 "200k" V 1375 9350 50  0001 C CNN
F 2 "Footprints:R_1206_3216Metric" V 1305 9350 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_AC1206FR-07200KL_C144506.html" H 1375 9350 50  0001 C CNN
	1    1375 9350
	0    -1   -1   0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R27
U 1 1 5BD04F97
P 1375 10000
F 0 "R27" V 1455 10000 50  0000 C CNN
F 1 "200k" V 1375 10000 50  0001 C CNN
F 2 "Footprints:R_1206_3216Metric" V 1305 10000 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_AC1206FR-07200KL_C144506.html" H 1375 10000 50  0001 C CNN
	1    1375 10000
	0    1    1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R20
U 1 1 5BD04FEE
P 5475 7850
F 0 "R20" V 5555 7850 50  0000 C CNN
F 1 "1k02" V 5475 7850 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5405 7850 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Others_YAGEO-AC0603FR-071K02L_C227642.html" H 5475 7850 50  0001 C CNN
	1    5475 7850
	0    1    -1   0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R23
U 1 1 5BD05047
P 6425 1450
F 0 "R23" V 6505 1450 50  0000 C CNN
F 1 "4k7" H 6425 1450 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6355 1450 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_4-7KR-472-1_C99782.html" H 6425 1450 50  0001 C CNN
	1    6425 1450
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R26
U 1 1 5BD050A2
P 1725 10650
F 0 "R26" V 1805 10650 50  0000 C CNN
F 1 "200k" V 1725 10650 50  0001 C CNN
F 2 "Footprints:R_1206_3216Metric" V 1655 10650 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_AC1206FR-07200KL_C144506.html" H 1725 10650 50  0001 C CNN
	1    1725 10650
	0    1    1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R28
U 1 1 5BD050FF
P 1375 10650
F 0 "R28" V 1455 10650 50  0000 C CNN
F 1 "200k" V 1375 10650 50  0001 C CNN
F 2 "Footprints:R_1206_3216Metric" V 1305 10650 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_AC1206FR-07200KL_C144506.html" H 1375 10650 50  0001 C CNN
	1    1375 10650
	0    1    1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R30
U 1 1 5BD0515E
P 1725 10000
F 0 "R30" V 1805 10000 50  0000 C CNN
F 1 "200k" V 1725 10000 50  0001 C CNN
F 2 "Footprints:R_1206_3216Metric" V 1655 10000 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_AC1206FR-07200KL_C144506.html" H 1725 10000 50  0001 C CNN
	1    1725 10000
	0    1    1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R31
U 1 1 5BD051BF
P 1725 9175
F 0 "R31" V 1805 9175 50  0000 C CNN
F 1 "2.2k" V 1725 9175 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1655 9175 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1725 9175 50  0001 C CNN
	1    1725 9175
	0    1    1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R32
U 1 1 5BD05222
P 1725 9825
F 0 "R32" V 1805 9825 50  0000 C CNN
F 1 "2.2k" V 1725 9825 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1655 9825 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1725 9825 50  0001 C CNN
	1    1725 9825
	0    1    1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R34
U 1 1 5BD05287
P 1725 10475
F 0 "R34" V 1805 10475 50  0000 C CNN
F 1 "2.2k" V 1725 10475 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1655 10475 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1725 10475 50  0001 C CNN
	1    1725 10475
	0    1    1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R35
U 1 1 5BD052EE
P 1900 8050
F 0 "R35" V 1980 8050 50  0000 C CNN
F 1 "4k7" V 1900 8050 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1830 8050 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_4-7KR-472-1_C99782.html" H 1900 8050 50  0001 C CNN
	1    1900 8050
	0    -1   1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R33
U 1 1 5BD0542F
P 4850 1450
F 0 "R33" V 4930 1450 50  0000 C CNN
F 1 "36k5" V 4850 1450 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 4780 1450 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Others_YAGEO-AC0603FR-0736K5L_C227840.html" H 4850 1450 50  0001 C CNN
	1    4850 1450
	-1   0    0    1   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R36
U 1 1 5BD0550F
P 5300 1425
F 0 "R36" V 5380 1425 50  0000 C CNN
F 1 "100k" V 5300 1425 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5230 1425 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_RC0603JR-07270KL_C137642.html" H 5300 1425 50  0001 C CNN
	1    5300 1425
	-1   0    0    1   
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C6
U 1 1 5BD060D2
P 4300 4275
F 0 "C6" H 4310 4345 50  0000 L CNN
F 1 "100nF 25v" H 4310 4195 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 4300 4275 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 4300 4275 50  0001 C CNN
	1    4300 4275
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C4
U 1 1 5BD0621D
P 4300 5550
F 0 "C4" H 4310 5620 50  0000 L CNN
F 1 "100nF 25v" H 4310 5470 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 4300 5550 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 4300 5550 50  0001 C CNN
	1    4300 5550
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C2
U 1 1 5BD06296
P 4300 6650
F 0 "C2" H 4310 6720 50  0000 L CNN
F 1 "100nF 25v" H 4310 6570 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 4300 6650 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 4300 6650 50  0001 C CNN
	1    4300 6650
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C3
U 1 1 5BD06311
P 5625 1500
F 0 "C3" H 5635 1570 50  0000 L CNN
F 1 "100nF 25v" H 5635 1420 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 5625 1500 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 5625 1500 50  0001 C CNN
	1    5625 1500
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C5
U 1 1 5BD0638E
P 4625 950
F 0 "C5" H 4635 1020 50  0000 L CNN
F 1 "100nF 25v" H 4635 870 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 4625 950 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 4625 950 50  0001 C CNN
	1    4625 950 
	0    1    1    0   
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C7
U 1 1 5BD06419
P 4275 7925
F 0 "C7" H 4285 7995 50  0000 L CNN
F 1 "100nF 25v" H 4285 7845 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 4275 7925 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 4275 7925 50  0001 C CNN
	1    4275 7925
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:CONN_01X04-3Phase-rescue-3Phase-rescue J1
U 1 1 5BD094EB
P 7175 4025
F 0 "J1" H 7325 4375 50  0000 C CNN
F 1 "95001-6P6C" H 6975 4375 50  0001 C CNN
F 2 "Footprints:KF2EDGR_2.54x4" H 7175 4025 50  0001 C CNN
F 3 "" H 7175 4025 50  0000 C CNN
F 4 "L1" H 7175 3750 50  0000 C CNN "Name"
	1    7175 4025
	1    0    0    1   
$EndComp
$Comp
L 3Phase:CONN_01X04-3Phase-rescue-3Phase-rescue J3
U 1 1 5BD09649
P 7225 6425
F 0 "J3" H 7375 6775 50  0000 C CNN
F 1 "95001-6P6C" H 7025 6775 50  0001 C CNN
F 2 "Footprints:KF2EDGR_2.54x4" H 7225 6425 50  0001 C CNN
F 3 "" H 7225 6425 50  0000 C CNN
F 4 "L3" H 7225 6675 50  0000 C CNN "Name"
	1    7225 6425
	1    0    0    1   
$EndComp
$Comp
L 3Phase:CONN_01X04-3Phase-rescue-3Phase-rescue J4
U 1 1 5BD096FE
P 7225 7700
F 0 "J4" H 7325 7875 50  0000 C CNN
F 1 "95001-6P6C" H 7025 8050 50  0001 C CNN
F 2 "Footprints:KF2EDGR_2.54x4" H 7225 7700 50  0001 C CNN
F 3 "" H 7225 7700 50  0000 C CNN
F 4 "N" H 7225 7400 50  0000 C CNN "Name"
	1    7225 7700
	1    0    0    1   
$EndComp
$Comp
L 3Phase:CONN_01X04-3Phase-rescue-3Phase-rescue J2
U 1 1 5BD095C2
P 7225 5275
F 0 "J2" H 7375 5625 50  0000 C CNN
F 1 "95001-6P6C" H 7025 5625 50  0001 C CNN
F 2 "Footprints:KF2EDGR_2.54x4" H 7225 5275 50  0001 C CNN
F 3 "" H 7225 5275 50  0000 C CNN
F 4 "L2" H 7225 5525 50  0000 C CNN "Name"
	1    7225 5275
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5BD125E9
P 4300 4400
F 0 "#PWR013" H 4300 4150 50  0001 C CNN
F 1 "GND" H 4300 4250 50  0000 C CNN
F 2 "" H 4300 4400 50  0000 C CNN
F 3 "" H 4300 4400 50  0000 C CNN
	1    4300 4400
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR032
U 1 1 5BD1A83E
P 6925 4175
F 0 "#PWR032" H 6925 3925 50  0001 C CNN
F 1 "GND" V 6850 4275 50  0000 C CNN
F 2 "" H 6925 4175 50  0000 C CNN
F 3 "" H 6925 4175 50  0000 C CNN
	1    6925 4175
	0    1    -1   0   
$EndComp
$Comp
L 3Phase:+1V65-Components #PWR021
U 1 1 5BD1DAD6
P 7200 4625
F 0 "#PWR021" H 7200 4475 50  0001 C CNN
F 1 "+1V65" V 7250 4525 50  0000 C CNN
F 2 "" H 7200 4625 50  0000 C CNN
F 3 "" H 7200 4625 50  0000 C CNN
	1    7200 4625
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR018
U 1 1 5BD1DD5A
P 5075 5650
F 0 "#PWR018" H 5075 5400 50  0001 C CNN
F 1 "GND" H 5175 5650 50  0000 C CNN
F 2 "" H 5075 5650 50  0000 C CNN
F 3 "" H 5075 5650 50  0000 C CNN
	1    5075 5650
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:+1V65-Components #PWR024
U 1 1 5BD26ECB
P 7200 5875
F 0 "#PWR024" H 7200 5725 50  0001 C CNN
F 1 "+1V65" V 7250 5750 50  0000 C CNN
F 2 "" H 7200 5875 50  0000 C CNN
F 3 "" H 7200 5875 50  0000 C CNN
	1    7200 5875
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5BD279CC
P 4300 5675
F 0 "#PWR014" H 4300 5425 50  0001 C CNN
F 1 "GND" H 4300 5525 50  0000 C CNN
F 2 "" H 4300 5675 50  0000 C CNN
F 3 "" H 4300 5675 50  0000 C CNN
	1    4300 5675
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR036
U 1 1 5BD27A56
P 7050 5425
F 0 "#PWR036" H 7050 5175 50  0001 C CNN
F 1 "GND" V 6950 5425 50  0000 C CNN
F 2 "" H 7050 5425 50  0000 C CNN
F 3 "" H 7050 5425 50  0000 C CNN
	1    7050 5425
	0    1    -1   0   
$EndComp
$Comp
L 3Phase:MCP6004-Components U3
U 2 1 5BD2D4E2
P 5000 6475
F 0 "U3" H 5000 6675 50  0000 L CNN
F 1 "MCP6004" H 4950 6275 50  0000 L CNN
F 2 "Footprints:SOIC-14_3.9x8.7mm_P1.27mm" H 4950 6575 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/74-Series_NXP_74HC4066PW_74HC4066PW_C5650.html" H 5050 6675 50  0001 C CNN
	2    5000 6475
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:MCP6004-Components U3
U 4 1 5BD2D609
P 5975 7850
F 0 "U3" H 5600 8075 50  0000 L CNN
F 1 "MCP6004" H 5825 7625 50  0000 L CNN
F 2 "Footprints:SOIC-14_3.9x8.7mm_P1.27mm" H 5925 7950 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/74-Series_NXP_74HC4066PW_74HC4066PW_C5650.html" H 6025 8050 50  0001 C CNN
	4    5975 7850
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:MCP6004-Components U3
U 1 1 5BD2D6F4
P 4950 7750
F 0 "U3" H 4950 7950 50  0000 L CNN
F 1 "MCP6004" H 4950 7550 50  0000 L CNN
F 2 "Footprints:SOIC-14_3.9x8.7mm_P1.27mm" H 4900 7850 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/74-Series_NXP_74HC4066PW_74HC4066PW_C5650.html" H 5000 7950 50  0001 C CNN
	1    4950 7750
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:MCP6004-Components U3
U 3 1 5BD2D7EF
P 5975 6575
F 0 "U3" H 6075 6675 50  0000 L CNN
F 1 "MCP6004" H 5850 6350 50  0000 L CNN
F 2 "Footprints:SOIC-14_3.9x8.7mm_P1.27mm" H 5925 6675 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/74-Series_NXP_74HC4066PW_74HC4066PW_C5650.html" H 6025 6775 50  0001 C CNN
	3    5975 6575
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:+1V65-Components #PWR022
U 1 1 5BD3C362
P 7200 7025
F 0 "#PWR022" H 7200 6875 50  0001 C CNN
F 1 "+1V65" V 7250 6925 50  0000 C CNN
F 2 "" H 7200 7025 50  0000 C CNN
F 3 "" H 7200 7025 50  0000 C CNN
	1    7200 7025
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR040
U 1 1 5BD3C576
P 7050 6575
F 0 "#PWR040" H 7050 6325 50  0001 C CNN
F 1 "GND" V 6950 6500 50  0000 C CNN
F 2 "" H 7050 6575 50  0000 C CNN
F 3 "" H 7050 6575 50  0000 C CNN
	1    7050 6575
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5BD3E154
P 4300 6775
F 0 "#PWR015" H 4300 6525 50  0001 C CNN
F 1 "GND" H 4300 6625 50  0000 C CNN
F 2 "" H 4300 6775 50  0000 C CNN
F 3 "" H 4300 6775 50  0000 C CNN
	1    4300 6775
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 5BD4142B
P 4275 8050
F 0 "#PWR016" H 4275 7800 50  0001 C CNN
F 1 "GND" H 4275 7900 50  0000 C CNN
F 2 "" H 4275 8050 50  0000 C CNN
F 3 "" H 4275 8050 50  0000 C CNN
	1    4275 8050
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:+1V65-Components #PWR023
U 1 1 5BD41EEF
P 7200 8300
F 0 "#PWR023" H 7200 8150 50  0001 C CNN
F 1 "+1V65" V 7275 8375 50  0000 C CNN
F 2 "" H 7200 8300 50  0000 C CNN
F 3 "" H 7200 8300 50  0000 C CNN
	1    7200 8300
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR020
U 1 1 5BD4205A
P 5050 8075
F 0 "#PWR020" H 5050 7825 50  0001 C CNN
F 1 "GND" H 5150 8075 50  0000 C CNN
F 2 "" H 5050 8075 50  0000 C CNN
F 3 "" H 5050 8075 50  0000 C CNN
	1    5050 8075
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR044
U 1 1 5BD42FF9
P 6975 7850
F 0 "#PWR044" H 6975 7600 50  0001 C CNN
F 1 "GND" V 6850 7800 50  0000 C CNN
F 2 "" H 6975 7850 50  0000 C CNN
F 3 "" H 6975 7850 50  0000 C CNN
	1    6975 7850
	0    1    -1   0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R46
U 1 1 5BD9FCD3
P 6650 6650
F 0 "R46" V 6730 6650 50  0000 C CNN
F 1 "270k" V 6650 6650 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6580 6650 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_RC0603JR-07270KL_C137642.html" H 6650 6650 50  0001 C CNN
	1    6650 6650
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R45
U 1 1 5BD9FD98
P 6800 6650
F 0 "R45" V 6880 6650 50  0000 C CNN
F 1 "270k" V 6800 6650 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6730 6650 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_RC0603JR-07270KL_C137642.html" H 6800 6650 50  0001 C CNN
	1    6800 6650
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R48
U 1 1 5BDAF4B1
P 6650 7925
F 0 "R48" V 6730 7925 50  0000 C CNN
F 1 "270k" V 6650 7925 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6580 7925 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_RC0603JR-07270KL_C137642.html" H 6650 7925 50  0001 C CNN
	1    6650 7925
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R47
U 1 1 5BDAF58A
P 6800 7925
F 0 "R47" V 6880 7925 50  0000 C CNN
F 1 "270k" V 6800 7925 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6730 7925 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_RC0603JR-07270KL_C137642.html" H 6800 7925 50  0001 C CNN
	1    6800 7925
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:CP-3Phase-rescue-3Phase-rescue C9
U 1 1 5BDB6F21
P 2375 1350
F 0 "C9" H 2400 1450 50  0000 L CNN
F 1 "4.7uF 450v (10x12mm)" H 2400 1250 50  0001 L CNN
F 2 "Footprints:CP_Radial_D10.0mm_P5.00mm" H 2413 1200 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Aluminum-Electrolytic-Capacitors-Leaded_4-7uF-20-450V_C106429.html" H 2375 1350 50  0001 C CNN
	1    2375 1350
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:CP-3Phase-rescue-3Phase-rescue C8
U 1 1 5BDB7050
P 1975 1350
F 0 "C8" H 2000 1450 50  0000 L CNN
F 1 "4.7uF 450v (10x12mm)" H 2000 1250 50  0001 L CNN
F 2 "Footprints:CP_Radial_D10.0mm_P5.00mm" H 2013 1200 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Aluminum-Electrolytic-Capacitors-Leaded_4-7uF-20-450V_C106429.html" H 1975 1350 50  0001 C CNN
	1    1975 1350
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:VIPer22-3Phase-rescue-3Phase-rescue U6
U 1 1 5BDB6955
P 2925 1250
F 0 "U6" H 2775 1475 50  0000 R CNN
F 1 "VIPER22ADIP-E" H 2675 925 50  0000 L CNN
F 2 "Footprints:DIP-8_W7.62mm" H 2925 675 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/PMIC-AC-DC-Converters_STMicroelectronics_VIPER22ADIP-E_VIPER22ADIP-E_C5318.html" H 2925 1250 50  0001 C CNN
	1    2925 1250
	-1   0    0    1   
$EndComp
$Comp
L 3Phase:L-3Phase-rescue-3Phase-rescue L1
U 1 1 5BDB7C0C
P 2175 1100
F 0 "L1" V 2125 1100 50  0000 C CNN
F 1 "470uH 350mA" H 2250 1100 50  0001 C CNN
F 2 "Footprints:L_Axial_L12.0mm_D5.0mm_P5.08mm_Vertical_Fastron_MISC" H 2175 1100 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_CENKER-CKL0514-470uH-J-CCA-T_C354673.html" H 2175 1100 50  0001 C CNN
	1    2175 1100
	0    -1   -1   0   
$EndComp
$Comp
L 3Phase:Thermistor_NTC-3Phase-rescue-3Phase-rescue TH1
U 1 1 5BDB864F
P 1775 950
F 0 "TH1" V 1600 950 50  0000 C CNN
F 1 "NTC 10D5" H 1900 950 50  0001 C CNN
F 2 "Footprints:RV_Disc_D7mm_W3.4mm_P5mm" H 1775 1000 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/NTC-Thermistors_Power-typeNTC-10R-20_C72681.html" H 1775 1000 50  0001 C CNN
	1    1775 950 
	0    1    1    0   
$EndComp
$Comp
L 3Phase:Varistor-3Phase-rescue-3Phase-rescue RV1
U 1 1 5BDB8784
P 1725 1350
F 0 "RV1" V 1850 1350 50  0000 C CNN
F 1 "05D391K" H 1600 1350 50  0001 C CNN
F 2 "Footprints:RV_Disc_D7mm_W3.4mm_P5mm" V 1655 1350 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Varistors_VDR-Varistor-VDR-05D391K_C283919.html" H 1725 1350 50  0001 C CNN
	1    1725 1350
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR011
U 1 1 5BDB9E15
P 3275 1700
F 0 "#PWR011" H 3275 1450 50  0001 C CNN
F 1 "GND" H 3275 1550 50  0000 C CNN
F 2 "" H 3275 1700 50  0000 C CNN
F 3 "" H 3275 1700 50  0000 C CNN
	1    3275 1700
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:L-3Phase-rescue-3Phase-rescue L2
U 1 1 5BDBAA07
P 3725 1350
F 0 "L2" V 3675 1350 50  0000 C CNN
F 1 "1mH 300mA" V 3800 1350 50  0001 C CNN
F 2 "Footprints:CD75_7.8x7.0" H 3725 1350 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Power-Inductors_COILMX-MSA75-102M_C396431.html" H 3725 1350 50  0001 C CNN
	1    3725 1350
	0    -1   -1   0   
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C10
U 1 1 5BDBB3A5
P 3525 1200
F 0 "C10" H 3535 1270 50  0000 L CNN
F 1 "100nF 25v" H 3535 1120 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 3525 1200 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3525 1200 50  0001 C CNN
	1    3525 1200
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C11
U 1 1 5BDBB5F6
P 3375 1250
F 0 "C11" H 3385 1320 50  0000 L CNN
F 1 "4.7uF 25v" H 3385 1170 50  0001 L CNN
F 2 "Footprints:C_0805_2012Metric" H 3375 1250 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_4-7uF-475-10-25V_C37818.html" H 3375 1250 50  0001 C CNN
	1    3375 1250
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:D_Zener_Small_ALT-3Phase-rescue-3Phase-rescue D2
U 1 1 5BDBB72B
P 3675 1050
F 0 "D2" H 3600 1150 50  0000 C CNN
F 1 "BZT52C18S" H 3675 960 50  0001 C CNN
F 2 "Footprints:D_SOD-323" V 3675 1050 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Zener-Diodes_Shandong-Jingdao-Microelectronics-BZT52C18S_C353551.html" V 3675 1050 50  0001 C CNN
	1    3675 1050
	-1   0    0    1   
$EndComp
$Comp
L 3Phase:D_Small_ALT-3Phase-rescue-3Phase-rescue D1
U 1 1 5BDBDB31
P 3275 1500
F 0 "D1" H 3225 1580 50  0000 L CNN
F 1 "ES1J" H 3125 1420 50  0001 L CNN
F 2 "Footprints:D_SMA" V 3275 1500 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Diodes-General-Purpose_Bourne-Semicon-Shenzhen-ES1J_C266561.html" H 3275 1500 50  0001 C CNN
	1    3275 1500
	0    1    1    0   
$EndComp
$Comp
L 3Phase:CP_Small-3Phase-rescue-3Phase-rescue C12
U 1 1 5BDBE803
P 3925 1500
F 0 "C12" H 3935 1570 50  0000 L CNN
F 1 "47uF 35v" H 3935 1420 50  0001 L CNN
F 2 "Footprints:CP_Radial_D5.0mm_P2.50mm" H 3925 1500 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Aluminum-Electrolytic-Capacitors-Leaded_Ymin-PKCB21V470MF_C269940.html" H 3925 1500 50  0001 C CNN
	1    3925 1500
	1    0    0    -1  
$EndComp
Text GLabel 4075 1100 1    60   Input ~ 0
20v
$Comp
L 3Phase:D_Small_ALT-3Phase-rescue-3Phase-rescue D3
U 1 1 5BDBD504
P 3925 1200
F 0 "D3" H 3875 1280 50  0000 L CNN
F 1 "ES1J" H 3775 1120 50  0001 L CNN
F 2 "Footprints:D_SMA" V 3925 1200 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Diodes-General-Purpose_Bourne-Semicon-Shenzhen-ES1J_C266561.html" V 3925 1200 50  0001 C CNN
	1    3925 1200
	0    1    1    0   
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C17
U 1 1 5BDCF00F
P 4075 1500
F 0 "C17" H 4085 1570 50  0000 L CNN
F 1 "100nF 25v" H 4085 1420 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 4075 1500 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 4075 1500 50  0001 C CNN
	1    4075 1500
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:L-3Phase-rescue-3Phase-rescue L4
U 1 1 5BDD2082
P 5025 1150
F 0 "L4" V 4975 1150 50  0000 C CNN
F 1 "10uH" V 5100 1150 50  0000 C CNN
F 2 "Footprints:CD43_4.5x4.0" H 5025 1150 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Power-Inductors_Guangdong-Fenghua-Advanced-Tech-PIO32-2R2MT_C143258.html" H 5025 1150 50  0001 C CNN
	1    5025 1150
	0    -1   -1   0   
$EndComp
Text GLabel 825  10650 0    60   Input ~ 0
IN_L3
$Comp
L 3Phase:D_ALT-3Phase-rescue-3Phase-rescue D6
U 1 1 5BDDF7B6
P 1375 1550
F 0 "D6" H 1475 1600 50  0000 C CNN
F 1 "1N4007G" H 1375 1450 50  0001 C CNN
F 2 "Footprints:D_SMA" H 1375 1550 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Diodes-General-Purpose_Shikues-1N4007G_C111122.html" H 1375 1550 50  0001 C CNN
	1    1375 1550
	-1   0    0    1   
$EndComp
$Comp
L 3Phase:D_ALT-3Phase-rescue-3Phase-rescue D4
U 1 1 5BDDF9F7
P 1375 950
F 0 "D4" H 1475 1000 50  0000 C CNN
F 1 "1N4007G" H 1375 850 50  0001 C CNN
F 2 "Footprints:D_SMA" H 1375 950 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Diodes-General-Purpose_Shikues-1N4007G_C111122.html" H 1375 950 50  0001 C CNN
	1    1375 950 
	-1   0    0    1   
$EndComp
$Comp
L 3Phase:D_ALT-3Phase-rescue-3Phase-rescue D5
U 1 1 5BDDFAD0
P 1375 1250
F 0 "D5" H 1475 1300 50  0000 C CNN
F 1 "1N4007G" H 1375 1150 50  0001 C CNN
F 2 "Footprints:D_SMA" H 1375 1250 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Diodes-General-Purpose_Shikues-1N4007G_C111122.html" H 1375 1250 50  0001 C CNN
	1    1375 1250
	-1   0    0    1   
$EndComp
Text GLabel 1225 800  2    60   Input ~ 0
IN_L3
Text GLabel 1225 1100 2    60   Input ~ 0
IN_L2
Text GLabel 1225 1400 2    60   Input ~ 0
IN_L1
$Comp
L 3Phase:CP_Small-3Phase-rescue-3Phase-rescue C18
U 1 1 5BDECD27
P 5450 1500
F 0 "C18" H 5460 1570 50  0000 L CNN
F 1 "100uF 16v" H 5460 1420 50  0001 L CNN
F 2 "Footprints:CP_Radial_D5.0mm_P2.50mm" H 5450 1500 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Aluminum-Electrolytic-Capacitors-Leaded_Ymin-PKCB41C101MF_C269939.html" H 5450 1500 50  0001 C CNN
	1    5450 1500
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:LTC4054-3Phase-rescue-3Phase-rescue U12
U 1 1 5BDEE248
P 5975 1250
F 0 "U12" H 5825 1475 50  0000 R CNN
F 1 "TP4054ST25P" H 6075 1475 50  0000 L CNN
F 2 "Footprints:SOT-23-5" H 5975 675 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/PMIC-Battery-Management_TPOWER-TP4054ST25P_C382138.html" H 5975 1250 50  0001 C CNN
	1    5975 1250
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:Battery_Cell-3Phase-rescue-3Phase-rescue BT1
U 1 1 5BDEE828
P 6625 1500
F 0 "BT1" H 6725 1600 50  0000 L CNN
F 1 "JST 2mm-2P" H 6725 1500 50  0001 L CNN
F 2 "Footprints:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" V 6625 1560 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Wire-To-Board-Wire-To-Wire-Connector_Boom-Precision-Elec-PH-2A_C2319.html" H 6625 1560 50  0001 C CNN
	1    6625 1500
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:Q_PMOS_GSD-3Phase-rescue-3Phase-rescue Q1
U 1 1 5BDF3EC9
P 6525 900
F 0 "Q1" H 6725 950 50  0000 L CNN
F 1 "AP2301" H 6725 850 50  0001 L CNN
F 2 "Footprints:SOT-23" H 6725 1000 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/MOSFET_ShenZhen-Quan-Li-Semiconductor-AP2301_C385296.html" H 6525 900 50  0001 C CNN
	1    6525 900 
	1    0    0    1   
$EndComp
$Comp
L 3Phase:D_Schottky_Small_ALT-3Phase-rescue-3Phase-rescue D8
U 1 1 5BDF4375
P 6450 650
F 0 "D8" H 6400 730 50  0000 L CNN
F 1 "SS14" H 6170 570 50  0001 L CNN
F 2 "Footprints:D_SMA" V 6450 650 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Schottky-Barrier-Diodes-SBD_Shandong-Jingdao-Microelectronics-SS12_C353243.html" V 6450 650 50  0001 C CNN
	1    6450 650 
	-1   0    0    1   
$EndComp
Text GLabel 5825 1775 0    60   Input ~ 0
CHRG
$Comp
L 3Phase:PS7516-3Phase-rescue-3Phase-rescue U7
U 1 1 5BE18D52
P 4625 9000
F 0 "U7" H 4825 8750 50  0000 C CNN
F 1 "PS7516" H 4525 8750 50  0000 C CNN
F 2 "Footprints:TSOT-23-6" H 4625 9100 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/DC-DC-Converters_FP6715S6CTR_C128048.html" H 4625 9100 50  0001 C CNN
	1    4625 9000
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:CONN_01X04-3Phase-rescue-3Phase-rescue P1
U 1 1 5BE1F723
P 775 1500
F 0 "P1" H 775 1750 50  0000 C CNN
F 1 "3.81x4 terminal block" V 875 1500 50  0000 C CNN
F 2 "Footprints:PhoenixContact_MC_1,5_4-G-3.81_1x04_P3.81mm_Horizontal" H 775 1500 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Pluggable-System-Terminal-Block_Ningbo-Kangnex-Elec-WJ15EDGRC-3-81-4P_C7245.html" H 775 1500 50  0001 C CNN
	1    775  1500
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R57
U 1 1 5BEA6C0B
P 7025 1400
F 0 "R57" V 7105 1400 50  0000 C CNN
F 1 "270k" H 7025 1400 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6955 1400 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_RC0603JR-07270KL_C137642.html" H 7025 1400 50  0001 C CNN
	1    7025 1400
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R56
U 1 1 5BEA6F94
P 6825 1150
F 0 "R56" V 6905 1150 50  0000 C CNN
F 1 "100k" H 6825 1150 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6755 1150 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_100KR-104-5_C100048.html" H 6825 1150 50  0001 C CNN
	1    6825 1150
	0    1    1    0   
$EndComp
Text GLabel 7250 1150 2    60   Input ~ 0
BAT_ADC
Text GLabel 4825 1775 0    60   Input ~ 0
EXT
Text GLabel 7075 650  2    60   Input ~ 0
POWER
Text GLabel 4125 9450 0    60   Input ~ 0
POWER
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C13
U 1 1 5BEB8358
P 4175 9650
F 0 "C13" H 4185 9720 50  0000 L CNN
F 1 "100nF 25v" H 4185 9570 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 4175 9650 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 4175 9650 50  0001 C CNN
	1    4175 9650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5BEB8531
P 3900 9800
F 0 "#PWR012" H 3900 9550 50  0001 C CNN
F 1 "GND" H 3900 9650 50  0000 C CNN
F 2 "" H 3900 9800 50  0000 C CNN
F 3 "" H 3900 9800 50  0000 C CNN
	1    3900 9800
	0    1    1    0   
$EndComp
Text GLabel 4150 9000 0    60   Input ~ 0
POWER_EN
$Comp
L 3Phase:L-3Phase-rescue-3Phase-rescue L3
U 1 1 5BEC97FD
P 4525 8650
F 0 "L3" V 4475 8650 50  0000 C CNN
F 1 "10uH" V 4600 8650 50  0000 C CNN
F 2 "Footprints:CD43_4.5x4.0" H 4525 8650 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Power-Inductors_Sunltech-Tech-SLF0403-100MTT_C182191.html" H 4525 8650 50  0001 C CNN
	1    4525 8650
	0    -1   -1   0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R50
U 1 1 5BEC99BD
P 5200 9275
F 0 "R50" V 5280 9275 50  0000 C CNN
F 1 "51k" V 5200 9275 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5130 9275 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_51KR-5102-1_C107231.html" H 5200 9275 50  0001 C CNN
	1    5200 9275
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R49
U 1 1 5BEC9B5A
P 5200 8850
F 0 "R49" V 5280 8850 50  0000 C CNN
F 1 "270k" V 5200 8850 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5130 8850 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_RC0603JR-07270KL_C137642.html" H 5200 8850 50  0001 C CNN
	1    5200 8850
	-1   0    0    1   
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C14
U 1 1 5BEC9CD0
P 5425 9575
F 0 "C14" H 5435 9645 50  0000 L CNN
F 1 "100nF 25v" H 5435 9495 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 5425 9575 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 5425 9575 50  0001 C CNN
	1    5425 9575
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:CP_Small-3Phase-rescue-3Phase-rescue C15
U 1 1 5BEC9E75
P 5325 9025
F 0 "C15" H 5335 9095 50  0000 L CNN
F 1 "100uF 16v" H 5335 8945 50  0001 L CNN
F 2 "Footprints:CP_Radial_D5.0mm_P2.50mm" H 5325 9025 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Aluminum-Electrolytic-Capacitors-Leaded_Ymin-PKCB41C101MF_C269939.html" H 5325 9025 50  0001 C CNN
	1    5325 9025
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:MCP6004-Components U11
U 1 1 5BEDF329
P 2225 9450
F 0 "U11" H 2225 9650 50  0000 L CNN
F 1 "MCP6004" H 2175 9250 50  0000 L CNN
F 2 "Footprints:SOIC-14_3.9x8.7mm_P1.27mm" H 2175 9550 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/74-Series_NXP_74HC4066PW_74HC4066PW_C5650.html" H 2275 9650 50  0001 C CNN
	1    2225 9450
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R51
U 1 1 5BEE0E5D
P 6725 9050
F 0 "R51" V 6805 9050 50  0000 C CNN
F 1 "4k7" V 6725 9050 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6655 9050 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_4-7KR-472-1_C99782.html" H 6725 9050 50  0001 C CNN
	1    6725 9050
	-1   0    0    1   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R52
U 1 1 5BEE103D
P 6600 9600
F 0 "R52" V 6680 9600 50  0000 C CNN
F 1 "4k7" V 6600 9600 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6530 9600 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_4-7KR-472-1_C99782.html" H 6600 9600 50  0001 C CNN
	1    6600 9600
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C16
U 1 1 5BEE335C
P 6725 9400
F 0 "C16" H 6735 9470 50  0000 L CNN
F 1 "100nF 25v" H 6735 9320 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 6725 9400 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 6725 9400 50  0001 C CNN
	1    6725 9400
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:+1V65-Components #PWR047
U 1 1 5BEEB00A
P 7600 9350
F 0 "#PWR047" H 7600 9200 50  0001 C CNN
F 1 "+1V65" V 7525 9350 50  0000 C CNN
F 2 "" H 7600 9350 50  0000 C CNN
F 3 "" H 7600 9350 50  0000 C CNN
	1    7600 9350
	0    1    1    0   
$EndComp
$Comp
L power:+3.3VA #PWR046
U 1 1 5BEED2DB
P 7300 8850
F 0 "#PWR046" H 7300 8700 50  0001 C CNN
F 1 "+3.3VA" V 7300 9100 50  0000 C CNN
F 2 "" H 7300 8850 50  0000 C CNN
F 3 "" H 7300 8850 50  0000 C CNN
	1    7300 8850
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR045
U 1 1 5BEED445
P 7325 8600
F 0 "#PWR045" H 7325 8450 50  0001 C CNN
F 1 "+5V" V 7325 8800 50  0000 C CNN
F 2 "" H 7325 8600 50  0000 C CNN
F 3 "" H 7325 8600 50  0000 C CNN
	1    7325 8600
	0    1    1    0   
$EndComp
Text GLabel 825  10000 0    60   Input ~ 0
IN_L2
Text GLabel 825  9350 0    60   Input ~ 0
IN_L1
Text GLabel 4275 4075 0    60   Input ~ 0
L1I_ADC
Text GLabel 4275 5325 0    60   Input ~ 0
L2I_ADC
Text GLabel 4275 6475 0    60   Input ~ 0
L3I_ADC
Text GLabel 4250 7750 0    60   Input ~ 0
NI_ADC
$Comp
L 3Phase:LED_Dual_ACA-Components D12
U 1 1 5BFAD7FE
P 2250 5050
F 0 "D12" H 1975 5000 50  0000 C CNN
F 1 "3mm bicolor LED" H 2250 4800 50  0001 C CNN
F 2 "Footprints:LED_D3.0mm" H 2250 5050 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100pcs-LED-3mm-Round-Diffused-Red-Green-two-Color-Common-Anode-cathode-LED-Diode-Light-Emitting/1758868_32887904538.html" H 2250 5050 50  0001 C CNN
	1    2250 5050
	-1   0    0    1   
$EndComp
$Comp
L 3Phase:LED_Dual_ACA-Components D10
U 1 1 5BFADB33
P 2250 5500
F 0 "D10" H 2025 5375 50  0000 C CNN
F 1 "3mm bicolor LED" H 2250 5250 50  0001 C CNN
F 2 "Footprints:LED_D3.0mm" H 2250 5500 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100pcs-LED-3mm-Round-Diffused-Red-Green-two-Color-Common-Anode-cathode-LED-Diode-Light-Emitting/1758868_32887904538.html" H 2250 5500 50  0001 C CNN
	1    2250 5500
	-1   0    0    1   
$EndComp
$Comp
L 3Phase:LED_Dual_ACA-Components D9
U 1 1 5BFADE7C
P 2250 5950
F 0 "D9" H 2025 5875 50  0000 C CNN
F 1 "3mm bicolor LED" H 2250 5700 50  0001 C CNN
F 2 "Footprints:LED_D3.0mm" H 2250 5950 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100pcs-LED-3mm-Round-Diffused-Red-Green-two-Color-Common-Anode-cathode-LED-Diode-Light-Emitting/1758868_32887904538.html" H 2250 5950 50  0001 C CNN
	1    2250 5950
	-1   0    0    1   
$EndComp
$Comp
L 3Phase:LED_ALT-3Phase-rescue-3Phase-rescue D11
U 1 1 5BFADFE9
P 2325 6250
F 0 "D11" H 2325 6350 50  0000 C CNN
F 1 "3mm blue LED" H 2325 6150 50  0001 C CNN
F 2 "Footprints:LED_D3.0mm" H 2325 6250 50  0001 C CNN
F 3 "https://es.aliexpress.com/store/product/100PCS-x-Long-Leg-3mm-5mm-White-Diffused-Jade-Green-Yellow-Red-Blue-Ultra-Bright-LED/102034_32887271791.html" H 2325 6250 50  0001 C CNN
	1    2325 6250
	-1   0    0    1   
$EndComp
$Comp
L 3Phase:Buzzer-3Phase-rescue-3Phase-rescue BZ1
U 1 1 5BFAE180
P 2125 2875
F 0 "BZ1" V 2000 2800 50  0000 L CNN
F 1 "QMB-09B-05" H 2275 2825 50  0001 L CNN
F 2 "Footprints:Buzzer_9x5.5RM4" V 2100 2975 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Buzzers_Jiangsu-Huaneng-Elec-QMB-09B-05_C161819.html" V 2100 2975 50  0001 C CNN
	1    2125 2875
	0    -1   -1   0   
$EndComp
$Comp
L 3Phase:NanoPi-Components P2
U 1 1 5BFAFAB2
P 1975 7500
F 0 "P2" H 2075 7150 50  0000 C CNN
F 1 "2 x 2.54x2 pin header" H 2075 7500 50  0001 C CNN
F 2 "Footprints:NanoPI Air" H 1975 7500 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Pin-Header-Female-Header_Boom-Precision-Elec-2-54-1-2PFemale_C49661.html" H 1975 7500 50  0001 C CNN
	1    1975 7500
	1    0    0    -1  
$EndComp
Text GLabel 3050 7000 2    60   Input ~ 0
L1I_ADC
Text GLabel 2650 6900 2    60   Input ~ 0
L2I_ADC
Text GLabel 3050 6800 2    60   Input ~ 0
L3I_ADC
$Comp
L 3Phase:MCP6004-Components U11
U 2 1 5BFE63CA
P 2225 10100
F 0 "U11" H 2225 10300 50  0000 L CNN
F 1 "MCP6004" H 2175 9900 50  0000 L CNN
F 2 "Footprints:SOIC-14_3.9x8.7mm_P1.27mm" H 2175 10200 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/74-Series_NXP_74HC4066PW_74HC4066PW_C5650.html" H 2275 10300 50  0001 C CNN
	2    2225 10100
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:MCP6004-Components U11
U 3 1 5BFEB564
P 2225 10750
F 0 "U11" H 2225 10950 50  0000 L CNN
F 1 "MCP6004" H 2175 10550 50  0000 L CNN
F 2 "Footprints:SOIC-14_3.9x8.7mm_P1.27mm" H 2175 10850 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/74-Series_NXP_74HC4066PW_74HC4066PW_C5650.html" H 2275 10950 50  0001 C CNN
	3    2225 10750
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:MCP6004-Components U11
U 4 1 5BFEBAD0
P 7100 9350
F 0 "U11" H 7100 9550 50  0000 L CNN
F 1 "MCP6004" H 7050 9150 50  0000 L CNN
F 2 "Footprints:SOIC-14_3.9x8.7mm_P1.27mm" H 7050 9450 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/74-Series_NXP_74HC4066PW_74HC4066PW_C5650.html" H 7150 9550 50  0001 C CNN
	4    7100 9350
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R62
U 1 1 5BFFA9B0
P 2725 9450
F 0 "R62" V 2805 9450 50  0000 C CNN
F 1 "100h" V 2725 9450 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 2655 9450 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_100R-100R-1_C125923.html" H 2725 9450 50  0001 C CNN
	1    2725 9450
	0    1    1    0   
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C19
U 1 1 5BFFE182
P 2900 9575
F 0 "C19" H 2910 9645 50  0000 L CNN
F 1 "100nF 25v" H 2910 9495 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 2900 9575 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 2900 9575 50  0001 C CNN
	1    2900 9575
	1    0    0    -1  
$EndComp
Text GLabel 3000 9450 2    60   Input ~ 0
L1V_ADC
$Comp
L power:GND #PWR08
U 1 1 5C00A67E
P 2900 9700
F 0 "#PWR08" H 2900 9450 50  0001 C CNN
F 1 "GND" H 2900 9550 50  0000 C CNN
F 2 "" H 2900 9700 50  0000 C CNN
F 3 "" H 2900 9700 50  0000 C CNN
	1    2900 9700
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:+1V65-Components #PWR01
U 1 1 5C00E7F5
P 1550 9175
F 0 "#PWR01" H 1550 9025 50  0001 C CNN
F 1 "+1V65" V 1550 9425 50  0000 C CNN
F 2 "" H 1550 9175 50  0000 C CNN
F 3 "" H 1550 9175 50  0000 C CNN
	1    1550 9175
	0    -1   -1   0   
$EndComp
$Comp
L 3Phase:+1V65-Components #PWR02
U 1 1 5C015D32
P 1550 9825
F 0 "#PWR02" H 1550 9675 50  0001 C CNN
F 1 "+1V65" V 1550 10075 50  0000 C CNN
F 2 "" H 1550 9825 50  0000 C CNN
F 3 "" H 1550 9825 50  0000 C CNN
	1    1550 9825
	0    -1   -1   0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R63
U 1 1 5C018829
P 2725 10100
F 0 "R63" V 2805 10100 50  0000 C CNN
F 1 "100h" V 2725 10100 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 2655 10100 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_100R-100R-1_C125923.html" H 2725 10100 50  0001 C CNN
	1    2725 10100
	0    1    1    0   
$EndComp
Text GLabel 3000 10100 2    60   Input ~ 0
L2V_ADC
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C20
U 1 1 5C018F55
P 2900 10225
F 0 "C20" H 2910 10295 50  0000 L CNN
F 1 "100nF 25v" H 2910 10145 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 2900 10225 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 2900 10225 50  0001 C CNN
	1    2900 10225
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5C01912D
P 2900 10350
F 0 "#PWR09" H 2900 10100 50  0001 C CNN
F 1 "GND" H 2900 10200 50  0000 C CNN
F 2 "" H 2900 10350 50  0000 C CNN
F 3 "" H 2900 10350 50  0000 C CNN
	1    2900 10350
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:+1V65-Components #PWR03
U 1 1 5C01CBFF
P 1550 10475
F 0 "#PWR03" H 1550 10325 50  0001 C CNN
F 1 "+1V65" V 1550 10725 50  0000 C CNN
F 2 "" H 1550 10475 50  0000 C CNN
F 3 "" H 1550 10475 50  0000 C CNN
	1    1550 10475
	0    -1   -1   0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R64
U 1 1 5C01E49B
P 2725 10750
F 0 "R64" V 2805 10750 50  0000 C CNN
F 1 "100h" V 2725 10750 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 2655 10750 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_100R-100R-1_C125923.html" H 2725 10750 50  0001 C CNN
	1    2725 10750
	0    1    1    0   
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C21
U 1 1 5C01E678
P 2900 10875
F 0 "C21" H 2910 10945 50  0000 L CNN
F 1 "100nF 25v" H 2910 10795 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 2900 10875 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 2900 10875 50  0001 C CNN
	1    2900 10875
	1    0    0    -1  
$EndComp
Text GLabel 3000 10750 2    60   Input ~ 0
L3V_ADC
$Comp
L power:GND #PWR010
U 1 1 5C020156
P 2900 11000
F 0 "#PWR010" H 2900 10750 50  0001 C CNN
F 1 "GND" H 2900 10850 50  0000 C CNN
F 2 "" H 2900 11000 50  0000 C CNN
F 3 "" H 2900 11000 50  0000 C CNN
	1    2900 11000
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:STM32F042-Components U13
U 1 1 5C0F1416
P 1250 8150
F 0 "U13" H 1250 7750 60  0000 C CNN
F 1 "STM32F042" H 1200 7625 60  0000 C CNN
F 2 "Footprints:LQFP-32_7x7mm_Pitch0.8mm" H 1250 8150 60  0001 C CNN
F 3 "https://lcsc.com/product-detail/ST-Microelectronics_STMicroelectronics_STM32F042K6T6_STM32F042K6T6_C69216.html" H 1250 8150 60  0001 C CNN
	1    1250 8150
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5C0F1AED
P 1625 8675
F 0 "#PWR04" H 1625 8425 50  0001 C CNN
F 1 "GND" H 1625 8525 50  0000 C CNN
F 2 "" H 1625 8675 50  0000 C CNN
F 3 "" H 1625 8675 50  0000 C CNN
	1    1625 8675
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3VA #PWR05
U 1 1 5C0F1C96
P 2275 8350
F 0 "#PWR05" H 2275 8200 50  0001 C CNN
F 1 "+3.3VA" V 2275 8600 50  0000 C CNN
F 2 "" H 2275 8350 50  0000 C CNN
F 3 "" H 2275 8350 50  0000 C CNN
	1    2275 8350
	0    1    1    0   
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C22
U 1 1 5C10FD29
P 1750 8500
F 0 "C22" H 1760 8570 50  0000 L CNN
F 1 "100nF 25v" H 1760 8420 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 1750 8500 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 1750 8500 50  0001 C CNN
	1    1750 8500
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C23
U 1 1 5C10FF28
P 1950 8500
F 0 "C23" H 1960 8570 50  0000 L CNN
F 1 "100nF 25v" H 1960 8420 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 1950 8500 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 1950 8500 50  0001 C CNN
	1    1950 8500
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C24
U 1 1 5C110430
P 2150 8500
F 0 "C24" H 2160 8570 50  0000 L CNN
F 1 "100nF 25v" H 2160 8420 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 2150 8500 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 2150 8500 50  0001 C CNN
	1    2150 8500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4275 4075 4300 4075
Wire Wire Line
	4325 4075 4300 4075
Connection ~ 4300 4075
Wire Notes Line
	3550 8500 7775 8500
Wire Notes Line
	3550 8925 475  8925
Wire Notes Line
	3550 2325 3550 9950
Wire Wire Line
	2900 11000 2900 10975
Connection ~ 2550 10750
Connection ~ 2900 10750
Wire Wire Line
	2900 10775 2900 10750
Wire Wire Line
	2875 10750 2900 10750
Wire Wire Line
	2525 10750 2550 10750
Wire Wire Line
	2550 11000 2550 10750
Wire Wire Line
	1900 11000 2550 11000
Wire Wire Line
	1900 10850 1900 11000
Wire Wire Line
	1925 10850 1900 10850
Wire Wire Line
	1550 10475 1575 10475
Connection ~ 1900 10650
Wire Wire Line
	1900 10475 1900 10650
Wire Wire Line
	1875 10475 1900 10475
Wire Wire Line
	2900 10325 2900 10350
Connection ~ 2900 10100
Wire Wire Line
	2900 10125 2900 10100
Wire Wire Line
	2875 10100 2900 10100
Wire Wire Line
	2550 10350 2550 10100
Wire Wire Line
	1900 10350 2550 10350
Wire Wire Line
	1900 10200 1900 10350
Wire Wire Line
	1925 10200 1900 10200
Wire Wire Line
	1550 9825 1575 9825
Connection ~ 1900 10000
Wire Wire Line
	1900 9825 1875 9825
Wire Wire Line
	1900 10000 1900 9825
Wire Wire Line
	1875 10000 1900 10000
Wire Wire Line
	1575 9175 1550 9175
Connection ~ 2900 9450
Wire Wire Line
	2900 9450 2900 9475
Wire Wire Line
	2875 9450 2900 9450
Connection ~ 1900 9350
Wire Wire Line
	1900 9175 1900 9350
Wire Wire Line
	1900 9175 1875 9175
Wire Wire Line
	1875 9350 1900 9350
Wire Wire Line
	825  10650 875  10650
Wire Wire Line
	875  10000 825  10000
Wire Wire Line
	825  9350 875  9350
Wire Wire Line
	5625 7850 5650 7850
Wire Wire Line
	5050 7450 5050 7425
Wire Wire Line
	5650 6575 5675 6575
Wire Wire Line
	6275 7750 6650 7750
Wire Wire Line
	4300 4375 4300 4400
Wire Wire Line
	5625 5425 5650 5425
Wire Wire Line
	1925 950  1975 950 
Connection ~ 4275 7750
Connection ~ 4300 6475
Connection ~ 7425 9350
Wire Wire Line
	7400 9350 7425 9350
Wire Wire Line
	7425 9725 7425 9350
Wire Wire Line
	6775 9725 7425 9725
Wire Wire Line
	6775 9450 6775 9725
Wire Wire Line
	6800 9450 6775 9450
Connection ~ 6725 9250
Connection ~ 6725 9800
Wire Wire Line
	6600 9250 6725 9250
Wire Wire Line
	6725 9200 6725 9250
Connection ~ 6725 8850
Wire Wire Line
	6600 9250 6600 9450
Connection ~ 6600 9800
Wire Wire Line
	6600 9800 6600 9750
Wire Wire Line
	6725 9800 6725 9500
Connection ~ 5425 9800
Connection ~ 4175 9450
Wire Wire Line
	4175 8650 4175 8900
Connection ~ 5200 9800
Wire Wire Line
	5200 9800 5200 9425
Connection ~ 5200 9100
Wire Wire Line
	5200 9000 5200 9100
Connection ~ 5200 8600
Wire Wire Line
	5200 8700 5200 8600
Connection ~ 5325 9800
Wire Wire Line
	5425 9800 5425 9675
Wire Wire Line
	5325 9800 5325 9125
Connection ~ 5325 8600
Wire Wire Line
	5325 8600 5325 8925
Connection ~ 5425 8600
Wire Wire Line
	5425 8600 5425 9475
Wire Wire Line
	5075 8600 5200 8600
Wire Wire Line
	5075 9000 5075 8600
Wire Wire Line
	4925 9000 5075 9000
Wire Wire Line
	4975 8650 4675 8650
Wire Wire Line
	4975 8900 4975 8650
Wire Wire Line
	4925 8900 4975 8900
Wire Wire Line
	4375 8650 4175 8650
Connection ~ 4175 9800
Wire Wire Line
	4175 9800 4175 9750
Connection ~ 4275 9800
Wire Wire Line
	4275 9100 4325 9100
Wire Wire Line
	4275 9800 4275 9100
Connection ~ 6625 650 
Connection ~ 7025 1150
Wire Wire Line
	6275 1150 6625 1150
Wire Wire Line
	7025 1150 7025 1250
Wire Wire Line
	6975 1150 7025 1150
Connection ~ 6625 1650
Wire Wire Line
	7025 1650 7025 1550
Wire Wire Line
	6425 1300 6425 1250
Wire Wire Line
	3525 1350 3525 1300
Connection ~ 2575 1100
Connection ~ 2575 1150
Wire Wire Line
	2575 1150 2625 1150
Connection ~ 2575 1250
Wire Wire Line
	2575 1250 2625 1250
Wire Wire Line
	2575 1050 2575 1100
Wire Wire Line
	2575 1350 2625 1350
Connection ~ 2375 1100
Wire Wire Line
	2375 1100 2375 1200
Wire Wire Line
	2575 1050 2625 1050
Wire Wire Line
	2325 1100 2375 1100
Connection ~ 1975 1100
Wire Wire Line
	1975 950  1975 1100
Wire Wire Line
	1725 1100 1725 1200
Wire Wire Line
	1725 1100 1975 1100
Connection ~ 1575 1250
Wire Wire Line
	1525 1250 1575 1250
Wire Wire Line
	1575 1550 1525 1550
Wire Wire Line
	1075 1350 975  1350
Wire Wire Line
	1125 1450 975  1450
Connection ~ 5625 1150
Wire Wire Line
	5625 900  5625 1150
Connection ~ 6625 1150
Wire Wire Line
	6325 1775 5825 1775
Wire Wire Line
	6325 1350 6325 1775
Wire Wire Line
	6275 1350 6325 1350
Wire Wire Line
	6625 650  6625 700 
Wire Wire Line
	6550 650  6625 650 
Wire Wire Line
	6350 650  6275 650 
Connection ~ 6275 900 
Wire Wire Line
	5625 900  6275 900 
Connection ~ 6425 1650
Wire Wire Line
	6425 1650 6425 1600
Wire Wire Line
	975  1650 1725 1650
Wire Wire Line
	6425 1250 6275 1250
Wire Wire Line
	6625 1100 6625 1150
Connection ~ 5975 1650
Wire Wire Line
	6625 1650 6625 1600
Connection ~ 5450 1650
Wire Wire Line
	5975 1650 5975 1500
Wire Wire Line
	5450 1650 5450 1600
Connection ~ 4075 1150
Connection ~ 1175 1550
Wire Wire Line
	1175 1400 1175 1550
Wire Wire Line
	1225 1400 1175 1400
Connection ~ 1125 1250
Wire Wire Line
	1125 1100 1125 1250
Wire Wire Line
	1225 1100 1125 1100
Connection ~ 1075 950 
Wire Wire Line
	1075 800  1075 950 
Wire Wire Line
	1225 800  1075 800 
Wire Wire Line
	975  1550 1175 1550
Wire Wire Line
	1075 950  1225 950 
Wire Wire Line
	1125 1250 1225 1250
Connection ~ 1575 950 
Wire Wire Line
	1575 950  1575 1250
Wire Wire Line
	1525 950  1575 950 
Connection ~ 1725 1650
Connection ~ 3275 1650
Wire Wire Line
	3275 1600 3275 1650
Connection ~ 4850 1650
Connection ~ 4525 1650
Wire Wire Line
	4850 1650 4850 1600
Wire Wire Line
	4825 950  4725 950 
Wire Wire Line
	4825 1150 4825 950 
Wire Wire Line
	4525 1000 4525 950 
Wire Wire Line
	4525 1650 4525 1500
Wire Wire Line
	4075 1150 4225 1150
Connection ~ 4075 1350
Wire Wire Line
	4075 1100 4075 1150
Connection ~ 4075 1650
Wire Wire Line
	4075 1650 4075 1600
Connection ~ 3925 1650
Connection ~ 3925 1050
Wire Wire Line
	3925 950  3925 1050
Wire Wire Line
	3775 1050 3925 1050
Connection ~ 3375 1150
Wire Wire Line
	3375 1150 3375 950 
Connection ~ 3525 1050
Wire Wire Line
	3525 1050 3525 1100
Wire Wire Line
	3225 1050 3525 1050
Wire Wire Line
	3225 1150 3375 1150
Wire Wire Line
	3875 1350 3925 1350
Connection ~ 1975 1650
Wire Wire Line
	1725 1650 1725 1500
Connection ~ 2375 1650
Wire Wire Line
	1975 1650 1975 1500
Wire Wire Line
	2375 1650 2375 1500
Wire Wire Line
	3925 1650 3925 1600
Connection ~ 3925 1350
Wire Wire Line
	3925 1300 3925 1350
Connection ~ 3375 1350
Connection ~ 3525 1350
Connection ~ 3275 1350
Wire Wire Line
	3225 1250 3275 1250
Wire Wire Line
	3275 1250 3275 1350
Wire Wire Line
	3225 1350 3275 1350
Connection ~ 6650 8300
Connection ~ 6650 7025
Wire Wire Line
	6650 5325 6650 5375
Wire Wire Line
	6800 4075 6800 4175
Wire Wire Line
	1875 10650 1900 10650
Wire Wire Line
	1575 10650 1525 10650
Wire Wire Line
	1525 10000 1575 10000
Wire Wire Line
	1525 9350 1575 9350
Wire Wire Line
	1175 9350 1225 9350
Wire Wire Line
	1175 10000 1225 10000
Wire Wire Line
	1175 10650 1225 10650
Wire Wire Line
	6975 7850 7000 7850
Wire Wire Line
	6650 8300 6625 8300
Wire Wire Line
	6650 8075 6650 8300
Wire Wire Line
	4275 8050 4275 8025
Wire Wire Line
	4300 7750 4275 7750
Wire Wire Line
	4250 7750 4275 7750
Connection ~ 5650 7850
Wire Wire Line
	5650 7850 5675 7850
Connection ~ 5300 7850
Wire Wire Line
	5300 8300 5300 7850
Wire Wire Line
	4300 6750 4300 6775
Wire Wire Line
	6650 7025 6625 7025
Wire Wire Line
	7025 6575 7050 6575
Connection ~ 5675 6575
Wire Wire Line
	4300 5650 4300 5675
Wire Wire Line
	5300 7025 5325 7025
Wire Wire Line
	5325 7025 5325 6575
Wire Wire Line
	4625 6475 4675 6475
Wire Wire Line
	4325 6475 4300 6475
Wire Wire Line
	4275 6475 4300 6475
Wire Wire Line
	7050 5425 7025 5425
Connection ~ 5650 5425
Wire Wire Line
	5650 5425 5675 5425
Connection ~ 4650 5325
Wire Wire Line
	4275 5325 4300 5325
Wire Wire Line
	4625 5325 4650 5325
Wire Wire Line
	6650 4625 6625 4625
Wire Wire Line
	6950 3875 6975 3875
Wire Wire Line
	6925 4175 6950 4175
Connection ~ 6300 4625
Wire Wire Line
	4625 4075 4650 4075
Wire Wire Line
	5650 4175 5675 4175
Wire Wire Line
	1600 8150 2150 8150
Wire Wire Line
	1600 8450 1625 8450
Wire Wire Line
	1600 8250 1950 8250
Wire Wire Line
	1750 8650 1750 8600
Wire Wire Line
	1625 8650 1750 8650
Connection ~ 1625 8650
Wire Wire Line
	1950 8600 1950 8650
Connection ~ 1750 8650
Wire Wire Line
	2150 8650 2150 8600
Connection ~ 1950 8650
Wire Wire Line
	1750 8400 1750 8350
Connection ~ 1750 8350
Wire Wire Line
	1925 7750 1600 7750
Wire Wire Line
	1600 7650 1925 7650
$Comp
L 3Phase:SW_Push-3Phase-rescue-3Phase-rescue SW2
U 1 1 5C13FA35
P 2175 4575
F 0 "SW2" H 2225 4675 50  0000 L CNN
F 1 "SW_Push" H 2175 4515 50  0001 C CNN
F 2 "Footprints:SW_Tactile_SKHH_Angled" H 2175 4775 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Tactile-Switches_BRIGHT-TSA067G80-250_C354960.html" H 2175 4775 50  0001 C CNN
	1    2175 4575
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5C1438E9
P 2425 4700
F 0 "#PWR07" H 2425 4450 50  0001 C CNN
F 1 "GND" V 2425 4475 50  0000 C CNN
F 2 "" H 2425 4700 50  0000 C CNN
F 3 "" H 2425 4700 50  0000 C CNN
	1    2425 4700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2575 5950 2550 5950
Text GLabel 2675 6700 2    60   Input ~ 0
NI_ADC
Text GLabel 1625 7300 2    60   Input ~ 0
L1V_ADC
Text GLabel 1625 7200 2    60   Input ~ 0
L2V_ADC
Text GLabel 1625 7100 2    60   Input ~ 0
L3V_ADC
Wire Wire Line
	5300 5875 5300 5425
Connection ~ 5300 5425
Wire Wire Line
	4325 5325 4300 5325
Connection ~ 4300 5325
Wire Wire Line
	5325 6300 5325 6375
Wire Wire Line
	5300 6575 5325 6575
Connection ~ 5325 6575
Connection ~ 4675 6475
Wire Wire Line
	5300 8300 5275 8300
Wire Wire Line
	4300 4075 4300 4175
Wire Wire Line
	2550 10750 2575 10750
Wire Wire Line
	2900 10750 3000 10750
Wire Wire Line
	1900 10650 1925 10650
Wire Wire Line
	2900 10100 3000 10100
Wire Wire Line
	2550 10100 2575 10100
Wire Wire Line
	1900 10000 1925 10000
Wire Wire Line
	2900 9450 3000 9450
Wire Wire Line
	1900 9350 1925 9350
Wire Wire Line
	5650 8150 5975 8150
Wire Wire Line
	5675 6875 5975 6875
Wire Wire Line
	5650 5725 5975 5725
Wire Wire Line
	5650 4475 5975 4475
Wire Wire Line
	4275 7750 4275 7825
Wire Wire Line
	4300 6475 4300 6550
Wire Wire Line
	7425 9350 7525 9350
Wire Wire Line
	6725 9250 6800 9250
Wire Wire Line
	6725 9250 6725 9300
Wire Wire Line
	6600 9800 6725 9800
Wire Wire Line
	5200 9800 5325 9800
Wire Wire Line
	5200 9100 5200 9125
Wire Wire Line
	5200 8600 5325 8600
Wire Wire Line
	5325 9800 5425 9800
Wire Wire Line
	5325 8600 5425 8600
Wire Wire Line
	4175 9800 4275 9800
Wire Wire Line
	6625 650  7075 650 
Wire Wire Line
	7025 1150 7175 1150
Wire Wire Line
	6625 1650 7025 1650
Wire Wire Line
	2575 1100 2575 1150
Wire Wire Line
	2575 1150 2575 1250
Wire Wire Line
	2575 1250 2575 1350
Wire Wire Line
	2375 1100 2575 1100
Wire Wire Line
	1975 1100 1975 1200
Wire Wire Line
	1975 1100 2025 1100
Wire Wire Line
	1575 1250 1575 1550
Wire Wire Line
	5625 1150 5675 1150
Wire Wire Line
	6625 1150 6675 1150
Wire Wire Line
	6625 1150 6625 1300
Wire Wire Line
	6275 900  6325 900 
Wire Wire Line
	6425 1650 6625 1650
Wire Wire Line
	5975 1650 6425 1650
Wire Wire Line
	4075 1150 4075 1350
Wire Wire Line
	1175 1550 1225 1550
Wire Wire Line
	1125 1250 1125 1450
Wire Wire Line
	1075 950  1075 1350
Wire Wire Line
	1575 950  1625 950 
Wire Wire Line
	1725 1650 1975 1650
Wire Wire Line
	3275 1650 3275 1700
Wire Wire Line
	3275 1650 3925 1650
Wire Wire Line
	4525 1650 4850 1650
Wire Wire Line
	4075 1350 4200 1350
Wire Wire Line
	4075 1350 4075 1400
Wire Wire Line
	4075 1650 4200 1650
Wire Wire Line
	3925 1650 4075 1650
Wire Wire Line
	3925 1050 3925 1100
Wire Wire Line
	3525 1050 3575 1050
Wire Wire Line
	1975 1650 2375 1650
Wire Wire Line
	2375 1650 3275 1650
Wire Wire Line
	3925 1350 4075 1350
Wire Wire Line
	3925 1350 3925 1400
Wire Wire Line
	3375 1350 3525 1350
Wire Wire Line
	3525 1350 3575 1350
Wire Wire Line
	3275 1350 3275 1400
Wire Wire Line
	3275 1350 3375 1350
Wire Wire Line
	6650 8300 6800 8300
Wire Wire Line
	6650 7750 6650 7775
Wire Wire Line
	6650 7025 6800 7025
Wire Wire Line
	6650 4625 6800 4625
Wire Wire Line
	5300 7850 5325 7850
Wire Wire Line
	5675 6575 5675 6875
Wire Wire Line
	5650 5425 5650 5725
Wire Wire Line
	4650 5325 4675 5325
Wire Wire Line
	6300 4625 6325 4625
Wire Wire Line
	1625 8450 1625 8550
Wire Wire Line
	1750 8650 1950 8650
Wire Wire Line
	1950 8650 2150 8650
Wire Wire Line
	2150 8150 2150 8250
Wire Wire Line
	5300 5425 5325 5425
Wire Wire Line
	4300 5325 4300 5450
Wire Wire Line
	5325 6575 5350 6575
Wire Wire Line
	4675 6475 4700 6475
Wire Wire Line
	1600 8350 1750 8350
Wire Wire Line
	5175 1150 5450 1150
Wire Wire Line
	5450 1650 5625 1650
Wire Wire Line
	4850 1650 5450 1650
Wire Wire Line
	5625 1400 5625 1150
Wire Wire Line
	5625 1600 5625 1650
Connection ~ 5625 1650
Wire Wire Line
	5625 1650 5975 1650
Wire Wire Line
	6275 650  6275 900 
Wire Wire Line
	5450 1400 5450 1250
Wire Wire Line
	5450 1250 5450 1150
Connection ~ 5450 1250
Connection ~ 5450 1150
Wire Wire Line
	5450 1150 5625 1150
Wire Wire Line
	3900 9800 4175 9800
Wire Wire Line
	4125 9450 4175 9450
Wire Wire Line
	4175 9550 4175 9450
Wire Wire Line
	4150 9000 4325 9000
Wire Wire Line
	4325 8900 4175 8900
Connection ~ 4175 8900
Wire Wire Line
	4175 8900 4175 9450
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C1
U 1 1 5C21A3EA
P 6525 9025
F 0 "C1" H 6535 9095 50  0000 L CNN
F 1 "100nF 25v" H 6535 8945 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 6525 9025 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 6525 9025 50  0001 C CNN
	1    6525 9025
	1    0    0    -1  
$EndComp
Wire Wire Line
	6525 8925 6525 8850
Connection ~ 6525 8850
Wire Wire Line
	6525 8850 6725 8850
Wire Wire Line
	5075 5025 5075 5000
$Comp
L 3Phase:MCP6004-Components U1
U 1 1 5BD04329
P 4975 5325
F 0 "U1" H 4975 5525 50  0000 L CNN
F 1 "MCP6004" H 4950 5175 50  0000 L CNN
F 2 "Footprints:SOIC-14_3.9x8.7mm_P1.27mm" H 4925 5425 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/74-Series_NXP_74HC4066PW_74HC4066PW_C5650.html" H 5025 5525 50  0001 C CNN
	1    4975 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5075 5650 5075 5625
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R38
U 1 1 5C1EE790
P 1875 4850
F 0 "R38" V 1955 4850 50  0000 C CNN
F 1 "39h" V 1875 4850 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1805 4850 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_RC0603JR-0739RL_C137630.html" H 1875 4850 50  0001 C CNN
	1    1875 4850
	0    -1   1    0   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R55
U 1 1 5C1EFC16
P 1775 6250
F 0 "R55" V 1855 6250 50  0000 C CNN
F 1 "220h" V 1775 6250 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1705 6250 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_220R-220R-1_C107696.html" H 1775 6250 50  0001 C CNN
	1    1775 6250
	0    -1   1    0   
$EndComp
Wire Wire Line
	1925 6250 2175 6250
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R65
U 1 1 5C207DDF
P 6800 4325
F 0 "R65" V 6880 4325 50  0000 C CNN
F 1 "270k" V 6800 4325 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6730 4325 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_RC0603JR-07270KL_C137642.html" H 6800 4325 50  0001 C CNN
	1    6800 4325
	1    0    0    1   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R66
U 1 1 5C207F21
P 6650 4325
F 0 "R66" V 6730 4325 50  0000 C CNN
F 1 "270k" V 6650 4325 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6580 4325 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_RC0603JR-07270KL_C137642.html" H 6650 4325 50  0001 C CNN
	1    6650 4325
	1    0    0    1   
$EndComp
Wire Wire Line
	6650 4625 6650 4475
Connection ~ 6650 4625
Wire Wire Line
	6800 4475 6800 4625
Connection ~ 6800 4625
Wire Wire Line
	6800 4625 6950 4625
Wire Wire Line
	7025 5125 7000 5125
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R41
U 1 1 5C1A194C
P 6650 5525
F 0 "R41" V 6575 5525 50  0000 C CNN
F 1 "270k" V 6650 5525 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6580 5525 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_RC0603JR-07270KL_C137642.html" H 6650 5525 50  0001 C CNN
	1    6650 5525
	1    0    0    1   
$EndComp
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R42
U 1 1 5C1A1A90
P 6800 5525
F 0 "R42" V 6725 5525 50  0000 C CNN
F 1 "270k" V 6800 5525 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6730 5525 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_RC0603JR-07270KL_C137642.html" H 6800 5525 50  0001 C CNN
	1    6800 5525
	1    0    0    1   
$EndComp
Wire Wire Line
	6275 5325 6650 5325
Wire Wire Line
	6625 5875 6650 5875
Wire Wire Line
	6800 5675 6800 5875
Connection ~ 6800 5875
Wire Wire Line
	6800 5875 7200 5875
Wire Wire Line
	6650 5675 6650 5875
Connection ~ 6650 5875
Wire Wire Line
	6650 5875 6800 5875
Wire Wire Line
	6650 6800 6650 7025
Wire Wire Line
	6800 6800 6800 7025
Connection ~ 6800 7025
Wire Wire Line
	6800 7025 7200 7025
Wire Wire Line
	5050 8050 5050 8075
Wire Wire Line
	7000 6275 7025 6275
Wire Wire Line
	6800 8075 6800 8300
Connection ~ 6800 8300
Wire Wire Line
	6800 8300 7000 8300
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C26
U 1 1 5BF092ED
P 5175 7425
F 0 "C26" H 5185 7495 50  0000 L CNN
F 1 "100nF 25v" H 5185 7345 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 5175 7425 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 5175 7425 50  0001 C CNN
	1    5175 7425
	0    1    -1   0   
$EndComp
Wire Wire Line
	5075 7425 5050 7425
Connection ~ 5050 7425
Wire Wire Line
	5050 7425 5050 7400
$Comp
L power:GND #PWR0103
U 1 1 5BFBA1DC
P 5625 7425
F 0 "#PWR0103" H 5625 7175 50  0001 C CNN
F 1 "GND" V 5525 7350 50  0000 C CNN
F 2 "" H 5625 7425 50  0000 C CNN
F 3 "" H 5625 7425 50  0000 C CNN
	1    5625 7425
	0    -1   1    0   
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C25
U 1 1 5BFF8F92
P 5200 5000
F 0 "C25" H 5210 5070 50  0000 L CNN
F 1 "100nF 25v" H 5210 4920 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 5200 5000 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 5200 5000 50  0001 C CNN
	1    5200 5000
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5C03434A
P 5325 5000
F 0 "#PWR0104" H 5325 4750 50  0001 C CNN
F 1 "GND" V 5225 4925 50  0000 C CNN
F 2 "" H 5325 5000 50  0000 C CNN
F 3 "" H 5325 5000 50  0000 C CNN
	1    5325 5000
	0    -1   1    0   
$EndComp
Wire Wire Line
	5100 5000 5075 5000
Connection ~ 5075 5000
Wire Wire Line
	5075 5000 5075 4950
$Comp
L power:GND #PWR0105
U 1 1 5C0CCC53
P 1900 7550
F 0 "#PWR0105" H 1900 7300 50  0001 C CNN
F 1 "GND" V 1900 7350 50  0000 C CNN
F 2 "" H 1900 7550 50  0000 C CNN
F 3 "" H 1900 7550 50  0000 C CNN
	1    1900 7550
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0106
U 1 1 5C0CECC4
P 1900 7450
F 0 "#PWR0106" H 1900 7300 50  0001 C CNN
F 1 "+5V" V 1900 7650 50  0000 C CNN
F 2 "" H 1900 7450 50  0000 C CNN
F 3 "" H 1900 7450 50  0000 C CNN
	1    1900 7450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1925 7450 1900 7450
Wire Wire Line
	1900 7550 1925 7550
$Comp
L Transistor_FET:2N7002 Q2
U 1 1 5C95A6F5
P 2125 3200
F 0 "Q2" H 2330 3246 50  0000 L CNN
F 1 "2N7002" H 2330 3155 50  0000 L CNN
F 2 "Footprints:SOT-23" H 2325 3125 50  0001 L CIN
F 3 "https://lcsc.com/product-detail/MOSFET_Guangdong-Hottech-2N7002_C181082.html" H 2125 3200 50  0001 L CNN
	1    2125 3200
	1    0    0    -1  
$EndComp
Wire Notes Line
	475  2325 7800 2325
Wire Wire Line
	2650 6900 2525 6900
Wire Wire Line
	3050 7000 2625 7000
Wire Wire Line
	1625 7100 1600 7100
Wire Wire Line
	1625 7300 1600 7300
Wire Wire Line
	1625 7200 1600 7200
Wire Wire Line
	1625 8650 1625 8675
Wire Wire Line
	1600 8550 1625 8550
Connection ~ 1625 8550
Wire Wire Line
	1625 8550 1625 8650
Text GLabel 2125 6600 2    60   Input ~ 0
BAT_ADC
Wire Wire Line
	2125 6600 1600 6600
Text GLabel 1750 6500 2    60   Input ~ 0
CHRG
Text GLabel 2250 3700 2    60   Input ~ 0
EXT
Wire Wire Line
	1750 6500 1600 6500
Wire Wire Line
	1625 6250 1600 6250
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C28
U 1 1 5C17FA70
P 2100 4700
F 0 "C28" H 2110 4770 50  0000 L CNN
F 1 "100nF 25v" H 2110 4620 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 2100 4700 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 2100 4700 50  0001 C CNN
	1    2100 4700
	0    1    -1   0   
$EndComp
Wire Wire Line
	2000 4700 1625 4700
Wire Wire Line
	1625 4575 1625 4700
Wire Wire Line
	2375 4575 2425 4575
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R44
U 1 1 5BF49798
P 1975 3825
F 0 "R44" V 2055 3825 50  0000 C CNN
F 1 "100k" H 1975 3825 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1905 3825 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_100KR-104-5_C100048.html" H 1975 3825 50  0001 C CNN
	1    1975 3825
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5BF49924
P 2300 3825
F 0 "#PWR0109" H 2300 3575 50  0001 C CNN
F 1 "GND" H 2275 3675 50  0000 C CNN
F 2 "" H 2300 3825 50  0000 C CNN
F 3 "" H 2300 3825 50  0000 C CNN
	1    2300 3825
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2125 3825 2200 3825
Wire Wire Line
	1825 3825 1650 3825
Wire Wire Line
	1650 3825 1650 4000
Wire Wire Line
	1650 4000 1600 4000
Wire Wire Line
	2250 3700 1600 3700
Text GLabel 1700 3600 2    60   Input ~ 0
POWER_EN
Wire Wire Line
	1700 3600 1650 3600
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R37
U 1 1 5C1A62AF
P 1950 3425
F 0 "R37" V 2030 3425 50  0000 C CNN
F 1 "100k" H 1950 3425 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1880 3425 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_100KR-104-5_C100048.html" H 1950 3425 50  0001 C CNN
	1    1950 3425
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5C1A6403
P 2325 3425
F 0 "#PWR0110" H 2325 3175 50  0001 C CNN
F 1 "GND" H 2200 3350 50  0000 C CNN
F 2 "" H 2325 3425 50  0000 C CNN
F 3 "" H 2325 3425 50  0000 C CNN
	1    2325 3425
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1800 3425 1650 3425
Wire Wire Line
	1650 3425 1650 3600
Connection ~ 1650 3600
Wire Wire Line
	1650 3600 1600 3600
Wire Wire Line
	2500 6250 2475 6250
Wire Wire Line
	1725 4850 1600 4850
Wire Wire Line
	2025 4850 2575 4850
Wire Wire Line
	2575 4850 2575 5050
Wire Wire Line
	2550 5500 2575 5500
Connection ~ 2575 5500
Wire Wire Line
	2575 5500 2575 5950
Wire Wire Line
	2550 5050 2575 5050
Connection ~ 2575 5050
Wire Wire Line
	2575 5050 2575 5500
Wire Wire Line
	1950 5050 1600 5050
Wire Wire Line
	1600 5500 1950 5500
Wire Wire Line
	1950 5950 1600 5950
Wire Wire Line
	1925 3200 1600 3200
Wire Wire Line
	2100 3425 2225 3425
Wire Wire Line
	2225 3400 2225 3425
Connection ~ 2225 3425
Wire Wire Line
	2225 3425 2325 3425
$Comp
L power:+5V #PWR0112
U 1 1 5CA1F349
P 2000 3025
F 0 "#PWR0112" H 2000 2875 50  0001 C CNN
F 1 "+5V" V 2000 3225 50  0000 C CNN
F 2 "" H 2000 3025 50  0000 C CNN
F 3 "" H 2000 3025 50  0000 C CNN
	1    2000 3025
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2000 3025 2025 3025
Wire Wire Line
	2025 3025 2025 2975
Wire Wire Line
	1750 8350 2275 8350
Wire Wire Line
	1950 8400 1950 8250
Wire Wire Line
	4925 9100 5200 9100
Wire Wire Line
	5425 8600 5550 8600
Connection ~ 5550 8600
Wire Wire Line
	5550 8600 7325 8600
Wire Wire Line
	5875 9750 5875 9800
Wire Wire Line
	5875 9800 6200 9800
Wire Wire Line
	6725 8900 6725 8850
Connection ~ 5875 9800
Connection ~ 6525 9800
Wire Wire Line
	6525 9800 6600 9800
$Comp
L power:+3.3V #PWR0113
U 1 1 5DD49C09
P 6225 9450
F 0 "#PWR0113" H 6225 9300 50  0001 C CNN
F 1 "+3.3V" V 6300 9475 50  0000 C CNN
F 2 "" H 6225 9450 50  0000 C CNN
F 3 "" H 6225 9450 50  0000 C CNN
	1    6225 9450
	0    1    1    0   
$EndComp
Wire Wire Line
	6225 9450 6200 9450
$Comp
L 3Phase:D_Small_ALT-3Phase-rescue-3Phase-rescue D13
U 1 1 5DFFCEE7
P 5550 8725
F 0 "D13" H 5500 8805 50  0000 L CNN
F 1 "1N5819WS" H 5400 8645 50  0001 L CNN
F 2 "Footprints:D_SOD-323" V 5550 8725 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Others_B5819WS_C191023.html" V 5550 8725 50  0001 C CNN
	1    5550 8725
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR0114
U 1 1 5E131EA4
P 2275 8250
F 0 "#PWR0114" H 2275 8100 50  0001 C CNN
F 1 "+3.3V" V 2275 8500 50  0000 C CNN
F 2 "" H 2275 8250 50  0000 C CNN
F 3 "" H 2275 8250 50  0000 C CNN
	1    2275 8250
	0    1    1    0   
$EndComp
Wire Wire Line
	1950 8250 2150 8250
Connection ~ 1950 8250
Connection ~ 2150 8250
Wire Wire Line
	2150 8250 2150 8400
Wire Wire Line
	2150 8250 2275 8250
Wire Wire Line
	2225 2975 2225 3000
$Comp
L 3Phase:Conn_01x03-Components J5
U 1 1 5E457107
P 2450 2600
F 0 "J5" H 2530 2642 50  0000 L CNN
F 1 "2.54x3 pin header" H 2530 2551 50  0000 L CNN
F 2 "Footprints:PinHeader_1x03_P2.54mm_Vertical" H 2450 2600 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Pin-Header-Female-Header_Boom-Precision-Elec-Header-2-54mm-1-3P_C49257.html" H 2450 2600 50  0001 C CNN
	1    2450 2600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5E55C7EA
P 1950 2700
F 0 "#PWR0108" H 1950 2450 50  0001 C CNN
F 1 "GND" V 1950 2525 50  0000 C CNN
F 2 "" H 1950 2700 50  0000 C CNN
F 3 "" H 1950 2700 50  0000 C CNN
	1    1950 2700
	0    1    1    0   
$EndComp
Wire Wire Line
	2250 2700 1950 2700
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R39
U 1 1 5E8C6875
P 1800 4575
F 0 "R39" V 1880 4575 50  0000 C CNN
F 1 "4k7" H 1800 4575 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1730 4575 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_4-7KR-472-1_C99782.html" H 1800 4575 50  0001 C CNN
	1    1800 4575
	0    1    1    0   
$EndComp
Wire Wire Line
	1650 4575 1625 4575
Wire Wire Line
	1950 4575 1975 4575
Wire Wire Line
	2050 8050 2150 8050
Wire Wire Line
	2150 8050 2150 8150
$Comp
L 3Phase:XC6206-3Phase-rescue-3Phase-rescue U4
U 1 1 5D1E085F
P 5875 9550
F 0 "U4" H 5975 9350 50  0000 C CNN
F 1 "ME6216A33M3G" H 5875 9850 50  0001 C CNN
F 2 "Footprints:SOT-23" H 5875 9650 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Low-Dropout-Regulators-LDO_ME6216A33M3G_C126466.html" H 5875 9650 50  0001 C CNN
	1    5875 9550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4275 9800 5050 9800
$Comp
L 3Phase:D_Small_ALT-3Phase-rescue-3Phase-rescue D14
U 1 1 5C0D8D38
P 4475 9450
F 0 "D14" H 4425 9530 50  0000 L CNN
F 1 "1N5819WS" H 4325 9370 50  0001 L CNN
F 2 "Footprints:D_SOD-323" V 4475 9450 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Others_B5819WS_C191023.html" V 4475 9450 50  0001 C CNN
	1    4475 9450
	-1   0    0    1   
$EndComp
Wire Wire Line
	4175 9450 4375 9450
Wire Wire Line
	4575 9450 5050 9450
Wire Wire Line
	5550 8825 5550 8850
Connection ~ 5550 9450
Wire Wire Line
	5550 9450 5575 9450
Connection ~ 2150 8150
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C29
U 1 1 5C6260A8
P 5050 9625
F 0 "C29" H 5060 9695 50  0000 L CNN
F 1 "100nF 25v" H 5060 9545 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 5050 9625 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 5050 9625 50  0001 C CNN
	1    5050 9625
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 9525 5050 9450
Connection ~ 5050 9450
Wire Wire Line
	5050 9450 5550 9450
Wire Wire Line
	5050 9725 5050 9800
Connection ~ 5050 9800
Wire Wire Line
	5050 9800 5200 9800
$Comp
L power:+3.3V #PWR0115
U 1 1 5C6C4D32
P 2425 4575
F 0 "#PWR0115" H 2425 4425 50  0001 C CNN
F 1 "+3.3V" V 2425 4800 50  0000 C CNN
F 2 "" H 2425 4575 50  0000 C CNN
F 3 "" H 2425 4575 50  0000 C CNN
	1    2425 4575
	0    1    1    0   
$EndComp
Wire Wire Line
	5550 8600 5550 8625
Wire Wire Line
	6525 9125 6525 9800
Connection ~ 6200 9450
Wire Wire Line
	6200 9450 6175 9450
Wire Wire Line
	2200 4700 2425 4700
Wire Wire Line
	1600 4700 1625 4700
Connection ~ 1625 4700
$Comp
L power:+5V #PWR0101
U 1 1 5D194434
P 5050 7400
F 0 "#PWR0101" H 5050 7250 50  0001 C CNN
F 1 "+5V" V 4975 7425 50  0000 C CNN
F 2 "" H 5050 7400 50  0000 C CNN
F 3 "" H 5050 7400 50  0000 C CNN
	1    5050 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6725 8850 7300 8850
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C30
U 1 1 5D19FBA6
P 2300 9125
F 0 "C30" H 2310 9195 50  0000 L CNN
F 1 "100nF 25v" H 2310 9045 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 2300 9125 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 2300 9125 50  0001 C CNN
	1    2300 9125
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5D19FF54
P 2475 9125
F 0 "#PWR0117" H 2475 8875 50  0001 C CNN
F 1 "GND" H 2475 8975 50  0000 C CNN
F 2 "" H 2475 9125 50  0000 C CNN
F 3 "" H 2475 9125 50  0000 C CNN
	1    2475 9125
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0118
U 1 1 5D1A14C3
P 2125 9100
F 0 "#PWR0118" H 2125 8950 50  0001 C CNN
F 1 "+5V" V 2200 9150 50  0000 C CNN
F 2 "" H 2125 9100 50  0000 C CNN
F 3 "" H 2125 9100 50  0000 C CNN
	1    2125 9100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2125 9100 2125 9125
Wire Wire Line
	2200 9125 2125 9125
Wire Wire Line
	2125 9125 2125 9150
Wire Wire Line
	2400 9125 2475 9125
$Comp
L power:+5V #PWR0119
U 1 1 5D28B512
P 5075 4950
F 0 "#PWR0119" H 5075 4800 50  0001 C CNN
F 1 "+5V" V 5150 5000 50  0000 C CNN
F 2 "" H 5075 4950 50  0000 C CNN
F 3 "" H 5075 4950 50  0000 C CNN
	1    5075 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4825 1150 4875 1150
$Comp
L Device:R_Small R40
U 1 1 5D62E090
P 4200 1525
F 0 "R40" H 4300 1550 50  0000 C CNN
F 1 "4k7" H 4200 1435 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 4200 1525 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_4-7KR-472-1_C99782.html" V 4200 1525 50  0001 C CNN
	1    4200 1525
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 1625 4200 1650
Connection ~ 4200 1650
Wire Wire Line
	4200 1650 4525 1650
Wire Wire Line
	4200 1425 4200 1350
Connection ~ 4200 1350
Wire Wire Line
	4200 1350 4225 1350
Wire Wire Line
	3375 950  3925 950 
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C27
U 1 1 5D4A1360
P 7525 9525
F 0 "C27" H 7535 9595 50  0000 L CNN
F 1 "100nF 25v" H 7535 9445 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 7525 9525 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 7525 9525 50  0001 C CNN
	1    7525 9525
	1    0    0    -1  
$EndComp
Wire Wire Line
	7525 9425 7525 9350
Connection ~ 7525 9350
Wire Wire Line
	7525 9350 7600 9350
Wire Wire Line
	7525 9625 7525 9800
Wire Wire Line
	2900 9700 2900 9675
Wire Wire Line
	6725 9800 7525 9800
Wire Wire Line
	1600 8050 1750 8050
$Comp
L 3Phase:CP_Small-3Phase-rescue-3Phase-rescue C31
U 1 1 5DA3AF22
P 6200 9650
F 0 "C31" H 6210 9720 50  0000 L CNN
F 1 "100uF 16v" H 6210 9570 50  0001 L CNN
F 2 "Footprints:CP_Radial_D5.0mm_P2.50mm" H 6200 9650 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Aluminum-Electrolytic-Capacitors-Leaded_Ymin-PKCB41C101MF_C269939.html" H 6200 9650 50  0001 C CNN
	1    6200 9650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5425 9800 5875 9800
Wire Wire Line
	6200 9750 6200 9800
Connection ~ 6200 9800
Wire Wire Line
	6200 9800 6400 9800
Wire Wire Line
	6200 9550 6200 9450
$Comp
L power:GND #PWR0102
U 1 1 5DB9F2FC
P 2125 9775
F 0 "#PWR0102" H 2125 9525 50  0001 C CNN
F 1 "GND" H 2250 9750 50  0000 C CNN
F 2 "" H 2125 9775 50  0000 C CNN
F 3 "" H 2125 9775 50  0000 C CNN
	1    2125 9775
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5C28DFF1
P 2500 6250
F 0 "#PWR0111" H 2500 6000 50  0001 C CNN
F 1 "GND" H 2375 6175 50  0000 C CNN
F 2 "" H 2500 6250 50  0000 C CNN
F 3 "" H 2500 6250 50  0000 C CNN
	1    2500 6250
	0    -1   -1   0   
$EndComp
$Comp
L Transistor_FET:2N7002 Q3
U 1 1 5D9F658F
P 2100 4050
F 0 "Q3" H 2305 4096 50  0000 L CNN
F 1 "2N7002" H 2305 4005 50  0000 L CNN
F 2 "Footprints:SOT-23" H 2300 3975 50  0001 L CIN
F 3 "https://lcsc.com/product-detail/MOSFET_Guangdong-Hottech-2N7002_C181082.html" H 2100 4050 50  0001 L CNN
	1    2100 4050
	1    0    0    1   
$EndComp
Wire Wire Line
	2200 3850 2200 3825
Connection ~ 2200 3825
Wire Wire Line
	2200 3825 2300 3825
Wire Wire Line
	1900 4050 1650 4050
Wire Wire Line
	1650 4050 1650 4000
Connection ~ 1650 4000
$Comp
L 3Phase:Conn_01x02-Components J6
U 1 1 5DB54C15
P 2525 4250
F 0 "J6" H 2605 4292 50  0000 L CNN
F 1 "JST 2mm-2P" H 2605 4201 50  0000 L CNN
F 2 "Footprints:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" H 2525 4250 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Wire-To-Board-Wire-To-Wire-Connector_Boom-Precision-Elec-PH-2A_C2319.html" H 2525 4250 50  0001 C CNN
	1    2525 4250
	1    0    0    1   
$EndComp
Wire Wire Line
	2200 4250 2325 4250
$Comp
L power:+5V #PWR0107
U 1 1 5DC70A3A
P 2300 4350
F 0 "#PWR0107" H 2300 4200 50  0001 C CNN
F 1 "+5V" V 2300 4550 50  0000 C CNN
F 2 "" H 2300 4350 50  0000 C CNN
F 3 "" H 2300 4350 50  0000 C CNN
	1    2300 4350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2325 4350 2300 4350
$Comp
L 3Phase:XC6206-3Phase-rescue-3Phase-rescue U2
U 1 1 5DDDB6A8
P 5875 8950
F 0 "U2" H 5975 8750 50  0000 C CNN
F 1 "ME6216A33M3G" H 5875 9250 50  0001 C CNN
F 2 "Footprints:SOT-23" H 5875 9050 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Low-Dropout-Regulators-LDO_ME6216A33M3G_C126466.html" H 5875 9050 50  0001 C CNN
	1    5875 8950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5575 8850 5550 8850
Connection ~ 5550 8850
Wire Wire Line
	5550 8850 5550 9450
Wire Wire Line
	6175 8850 6400 8850
$Comp
L power:GND #PWR0116
U 1 1 5E58F5CC
P 5875 9175
F 0 "#PWR0116" H 5875 8925 50  0001 C CNN
F 1 "GND" H 5750 9075 50  0000 C CNN
F 2 "" H 5875 9175 50  0000 C CNN
F 3 "" H 5875 9175 50  0000 C CNN
	1    5875 9175
	1    0    0    -1  
$EndComp
Wire Wire Line
	5875 9150 5875 9175
$Comp
L 3Phase:CP_Small-3Phase-rescue-3Phase-rescue C32
U 1 1 5E5D681A
P 6400 9225
F 0 "C32" H 6410 9295 50  0000 L CNN
F 1 "100uF 16v" H 6410 9145 50  0001 L CNN
F 2 "Footprints:CP_Radial_D5.0mm_P2.50mm" H 6400 9225 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Aluminum-Electrolytic-Capacitors-Leaded_Ymin-PKCB41C101MF_C269939.html" H 6400 9225 50  0001 C CNN
	1    6400 9225
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 9125 6400 8850
Connection ~ 6400 8850
Wire Wire Line
	6400 8850 6525 8850
Wire Wire Line
	6400 9325 6400 9800
Connection ~ 6400 9800
Wire Wire Line
	6400 9800 6525 9800
Wire Wire Line
	6800 7575 5275 7575
Wire Wire Line
	5275 7575 5275 7650
Wire Wire Line
	5650 7850 5650 8150
Wire Wire Line
	6800 7575 6800 7750
Wire Wire Line
	7025 7750 6800 7750
Connection ~ 6800 7750
Wire Wire Line
	6800 7750 6800 7775
Wire Wire Line
	6650 7750 6650 7650
Wire Wire Line
	6650 7650 7025 7650
Connection ~ 6650 7750
Wire Wire Line
	4600 7750 4625 7750
Wire Wire Line
	4975 8300 4625 8300
Wire Wire Line
	4625 8300 4625 7750
Connection ~ 4625 7750
Wire Wire Line
	4625 7750 4650 7750
Wire Wire Line
	6800 6300 6800 6475
Wire Wire Line
	5325 6300 6800 6300
Wire Wire Line
	7025 6475 6800 6475
Connection ~ 6800 6475
Wire Wire Line
	6800 6475 6800 6500
Wire Wire Line
	7025 6375 6650 6375
Wire Wire Line
	6650 6375 6650 6475
Wire Wire Line
	6275 6475 6650 6475
Connection ~ 6650 6475
Wire Wire Line
	6650 6475 6650 6500
Wire Wire Line
	5000 7025 4675 7025
Wire Wire Line
	4675 6475 4675 7025
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C33
U 1 1 5F640827
P 7000 8150
F 0 "C33" H 7010 8220 50  0000 L CNN
F 1 "100nF 25v" H 7010 8070 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 7000 8150 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 7000 8150 50  0001 C CNN
	1    7000 8150
	1    0    0    1   
$EndComp
Wire Wire Line
	7000 8050 7000 7850
Connection ~ 7000 7850
Wire Wire Line
	7000 7850 7025 7850
Wire Wire Line
	7000 8250 7000 8300
Connection ~ 7000 8300
Wire Wire Line
	7000 8300 7200 8300
Wire Wire Line
	6800 5150 5300 5150
Wire Wire Line
	5300 5150 5300 5225
Wire Wire Line
	7025 5325 6800 5325
Wire Wire Line
	6800 5325 6800 5375
Wire Wire Line
	6800 5150 6800 5325
Connection ~ 6800 5325
Wire Wire Line
	7025 5225 6650 5225
Wire Wire Line
	6650 5225 6650 5325
Connection ~ 6650 5325
Wire Wire Line
	5125 5875 5300 5875
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C34
U 1 1 5FB7F377
P 6950 4425
F 0 "C34" H 6960 4495 50  0000 L CNN
F 1 "100nF 25v" H 6960 4345 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 6950 4425 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 6950 4425 50  0001 C CNN
	1    6950 4425
	1    0    0    1   
$EndComp
Wire Wire Line
	6950 4525 6950 4625
Connection ~ 6950 4625
Wire Wire Line
	6950 4625 7200 4625
Wire Wire Line
	6950 4325 6950 4175
Connection ~ 6950 4175
Wire Wire Line
	6950 4175 6975 4175
Wire Wire Line
	6975 4075 6800 4075
Wire Wire Line
	6800 4075 6800 3900
Connection ~ 6800 4075
Wire Wire Line
	6975 3975 6650 3975
Wire Wire Line
	6650 3975 6650 4075
Wire Wire Line
	6275 4075 6650 4075
Connection ~ 6650 4075
Wire Wire Line
	6650 4075 6650 4175
Wire Wire Line
	1600 2600 2050 2600
Wire Wire Line
	2050 2600 2050 2500
Wire Wire Line
	2050 2500 2250 2500
Wire Wire Line
	2250 2600 2125 2600
Wire Wire Line
	2125 2600 2125 2550
Wire Wire Line
	2125 2550 1975 2550
Wire Wire Line
	1975 2550 1975 2500
Wire Wire Line
	1975 2500 1600 2500
Connection ~ 2550 10100
Wire Wire Line
	2525 10100 2550 10100
Connection ~ 2125 9125
$Comp
L 3Phase:XC6206-3Phase-rescue-3Phase-rescue U5
U 1 1 5D7CD025
P 6300 3300
F 0 "U5" H 6400 3100 50  0000 C CNN
F 1 "ME6216A33M3G" H 6300 3600 50  0001 C CNN
F 2 "Footprints:SOT-23" H 6300 3400 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Low-Dropout-Regulators-LDO_ME6216A33M3G_C126466.html" H 6300 3400 50  0001 C CNN
	1    6300 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0120
U 1 1 5D7CD1F3
P 6300 3550
F 0 "#PWR0120" H 6300 3300 50  0001 C CNN
F 1 "GND" H 6175 3450 50  0000 C CNN
F 2 "" H 6300 3550 50  0000 C CNN
F 3 "" H 6300 3550 50  0000 C CNN
	1    6300 3550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0121
U 1 1 5D7CDA10
P 5925 3200
F 0 "#PWR0121" H 5925 3050 50  0001 C CNN
F 1 "+5V" V 5925 3375 50  0000 C CNN
F 2 "" H 5925 3200 50  0000 C CNN
F 3 "" H 5925 3200 50  0000 C CNN
	1    5925 3200
	0    -1   1    0   
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C35
U 1 1 5D7CE217
P 5950 3375
F 0 "C35" H 5960 3445 50  0000 L CNN
F 1 "100nF 25v" H 5960 3295 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 5950 3375 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 5950 3375 50  0001 C CNN
	1    5950 3375
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C36
U 1 1 5D7CE3B7
P 6625 3375
F 0 "C36" H 6635 3445 50  0000 L CNN
F 1 "100nF 25v" H 6635 3295 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 6625 3375 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 6625 3375 50  0001 C CNN
	1    6625 3375
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6300 3500 6300 3525
Wire Wire Line
	5950 3475 5950 3525
Wire Wire Line
	5950 3525 6300 3525
Connection ~ 6300 3525
Wire Wire Line
	6300 3525 6300 3550
Wire Wire Line
	6625 3475 6625 3525
Wire Wire Line
	6625 3525 6300 3525
Wire Wire Line
	6625 3275 6625 3200
Wire Wire Line
	6625 3200 6600 3200
Wire Wire Line
	5925 3200 5950 3200
Wire Wire Line
	5950 3275 5950 3200
Connection ~ 5950 3200
Wire Wire Line
	5950 3200 6000 3200
$Comp
L Device:Jumper_NC_Dual JP1
U 1 1 5D9A3B6B
P 7025 3200
F 0 "JP1" H 7025 3439 50  0000 C CNN
F 1 "2.54x3 pin header" H 7025 3348 50  0000 C CNN
F 2 "Footprints:PinHeader_1x03_P2.54mm_Vertical" H 7025 3200 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Pin-Header-Female-Header_Boom-Precision-Elec-Header-2-54mm-1-3P_C49257.html" H 7025 3200 50  0001 C CNN
F 4 "" H 7025 3200 50  0001 C CNN "Additional"
	1    7025 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6775 3200 6625 3200
Connection ~ 6625 3200
Wire Wire Line
	5950 3200 5950 2825
Wire Wire Line
	5950 2825 7350 2825
Wire Wire Line
	7350 3200 7275 3200
Wire Wire Line
	7025 3300 7350 3300
Wire Wire Line
	7350 7450 7000 7450
Wire Wire Line
	7000 7450 7000 7550
Wire Wire Line
	7000 7550 7025 7550
Wire Wire Line
	7000 6275 7000 6175
Wire Wire Line
	7000 5125 7000 5000
Wire Wire Line
	7000 5000 7350 5000
Wire Wire Line
	6950 3875 6950 3750
Wire Wire Line
	6950 3750 7350 3750
$Comp
L 3Phase:+1V65-Components #PWR0122
U 1 1 5D8A650F
P 1775 4300
F 0 "#PWR0122" H 1775 4150 50  0001 C CNN
F 1 "+1V65" V 1825 4200 50  0000 C CNN
F 2 "" H 1775 4300 50  0000 C CNN
F 3 "" H 1775 4300 50  0000 C CNN
	1    1775 4300
	0    1    1    0   
$EndComp
$Comp
L 3Phase:C_Small-3Phase-rescue-3Phase-rescue C37
U 1 1 5D8F9F1D
P 7175 1525
F 0 "C37" H 7185 1595 50  0000 L CNN
F 1 "100nF 25v" H 7185 1445 50  0001 L CNN
F 2 "Footprints:R_0603_1608Metric" H 7175 1525 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 7175 1525 50  0001 C CNN
	1    7175 1525
	1    0    0    -1  
$EndComp
Wire Wire Line
	7175 1425 7175 1150
Connection ~ 7175 1150
Wire Wire Line
	7175 1150 7250 1150
Wire Wire Line
	7175 1625 7175 1650
Wire Wire Line
	7175 1650 7025 1650
Connection ~ 7025 1650
Wire Wire Line
	1775 4300 1600 4300
Wire Wire Line
	1925 9550 1900 9550
Wire Wire Line
	1900 9550 1900 9700
Wire Wire Line
	2550 9450 2575 9450
Wire Wire Line
	2525 9450 2550 9450
Connection ~ 2550 9450
Wire Wire Line
	2550 9700 2550 9450
Wire Wire Line
	2125 9775 2125 9750
Wire Wire Line
	1900 9700 2550 9700
$Comp
L 3Phase:4066-Components U9
U 2 1 5DC34F52
P 5525 8350
F 0 "U9" H 5644 8591 50  0000 C CNN
F 1 "74HC4066" H 5644 8500 50  0000 C CNN
F 2 "Footprints:TSSOP-14_4.4x5mm_P0.65mm" H 5525 8350 60  0001 C CNN
F 3 "https://lcsc.com/product-detail/74-Series_NXP_74HC4066PW_74HC4066PW_C5650.html" H 5525 8350 60  0001 C CNN
	2    5525 8350
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:4066-Components U9
U 3 1 5DC35198
P 5500 4675
F 0 "U9" H 5619 4916 50  0000 C CNN
F 1 "74HC4066" H 5619 4825 50  0000 C CNN
F 2 "Footprints:TSSOP-14_4.4x5mm_P0.65mm" H 5500 4675 60  0001 C CNN
F 3 "https://lcsc.com/product-detail/74-Series_NXP_74HC4066PW_74HC4066PW_C5650.html" H 5500 4675 60  0001 C CNN
	3    5500 4675
	1    0    0    -1  
$EndComp
$Comp
L 3Phase:4066-Components U9
U 4 1 5DC353B5
P 5500 5925
F 0 "U9" H 5475 6125 50  0000 C CNN
F 1 "74HC4066" H 5500 6050 50  0000 C CNN
F 2 "Footprints:TSSOP-14_4.4x5mm_P0.65mm" H 5500 5925 60  0001 C CNN
F 3 "https://lcsc.com/product-detail/74-Series_NXP_74HC4066PW_74HC4066PW_C5650.html" H 5500 5925 60  0001 C CNN
	4    5500 5925
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase:4066-Components U9
U 1 1 5DC355AF
P 5525 7075
F 0 "U9" H 5325 7225 50  0000 L CNN
F 1 "74HC4066" H 5275 7175 50  0000 L CNN
F 2 "Footprints:TSSOP-14_4.4x5mm_P0.65mm" H 5525 7075 60  0001 C CNN
F 3 "https://lcsc.com/product-detail/74-Series_NXP_74HC4066PW_74HC4066PW_C5650.html" H 5525 7075 60  0001 C CNN
	1    5525 7075
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6275 7950 6300 7950
Wire Wire Line
	6300 7950 6300 8150
Wire Wire Line
	6300 8300 6325 8300
Wire Wire Line
	6275 8150 6300 8150
Connection ~ 6300 8150
Wire Wire Line
	6300 8150 6300 8300
Wire Wire Line
	5350 8300 5300 8300
Connection ~ 5300 8300
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R58
U 1 1 5E3DE56F
P 6125 8300
F 0 "R58" V 6205 8300 50  0000 C CNN
F 1 "1k43" V 6125 8300 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6055 8300 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_1430R-1431-1_C185360.html" H 6125 8300 50  0001 C CNN
	1    6125 8300
	0    -1   1    0   
$EndComp
Wire Wire Line
	6275 8300 6300 8300
Connection ~ 6300 8300
Wire Wire Line
	5975 8300 5700 8300
Wire Wire Line
	7000 6175 7350 6175
Wire Wire Line
	6275 6675 6300 6675
Wire Wire Line
	6300 6675 6300 6875
Wire Wire Line
	6300 7025 6325 7025
Wire Wire Line
	6275 6875 6300 6875
Connection ~ 6300 6875
Wire Wire Line
	6300 6875 6300 7025
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R54
U 1 1 5E896D74
P 6125 7025
F 0 "R54" V 6175 7200 50  0000 C CNN
F 1 "1k43" V 6125 7025 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6055 7025 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_1430R-1431-1_C185360.html" H 6125 7025 50  0001 C CNN
	1    6125 7025
	0    -1   1    0   
$EndComp
Wire Wire Line
	6275 7025 6300 7025
Connection ~ 6300 7025
Wire Wire Line
	5975 7025 5700 7025
Wire Wire Line
	5250 7650 5275 7650
Wire Wire Line
	5250 7850 5300 7850
Wire Wire Line
	5625 7425 5525 7425
Wire Wire Line
	5300 6375 5325 6375
Wire Wire Line
	5350 7025 5325 7025
Connection ~ 5325 7025
Wire Wire Line
	6275 5525 6300 5525
Wire Wire Line
	6300 5525 6300 5725
Wire Wire Line
	6300 5875 6325 5875
Wire Wire Line
	6275 5725 6300 5725
Connection ~ 6300 5725
Wire Wire Line
	6300 5725 6300 5875
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R53
U 1 1 5FBED156
P 6125 5875
F 0 "R53" V 6205 5875 50  0000 C CNN
F 1 "1k43" V 6125 5875 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6055 5875 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_1430R-1431-1_C185360.html" H 6125 5875 50  0001 C CNN
	1    6125 5875
	0    -1   1    0   
$EndComp
Wire Wire Line
	6275 5875 6300 5875
Connection ~ 6300 5875
Wire Wire Line
	7350 6175 7350 7450
Wire Wire Line
	7350 5000 7350 6175
Wire Wire Line
	5325 5875 5300 5875
Connection ~ 5300 5875
Wire Wire Line
	4650 5875 4825 5875
Wire Wire Line
	4650 5325 4650 5875
Wire Wire Line
	5275 5225 5300 5225
Wire Wire Line
	5275 5425 5300 5425
Wire Wire Line
	5325 5000 5300 5000
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R43
U 1 1 5DBCF2ED
P 6125 4625
F 0 "R43" V 6205 4625 50  0000 C CNN
F 1 "1k43" V 6125 4625 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6055 4625 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_1430R-1431-1_C185360.html" H 6125 4625 50  0001 C CNN
	1    6125 4625
	0    -1   1    0   
$EndComp
Wire Wire Line
	6275 4625 6300 4625
Wire Wire Line
	5650 4175 5650 4475
Wire Wire Line
	5675 4625 5975 4625
Connection ~ 2625 7000
Wire Wire Line
	2625 7000 1600 7000
Connection ~ 2525 6900
Wire Wire Line
	2525 6900 1600 6900
Wire Wire Line
	1600 6700 2325 6700
Connection ~ 2325 6700
Wire Wire Line
	2325 6700 2675 6700
$Comp
L 3Phase:SRV05-4-Components U8
U 1 1 5E402EB0
P 2725 7375
F 0 "U8" H 2725 7058 50  0000 C CNN
F 1 "SRV05-4" H 2725 7149 50  0000 C CNN
F 2 "Footprints:SOT-23-6" H 2800 6850 50  0001 C TNN
F 3 "https://lcsc.com/product-detail/TVS_Leiditech-SRV05-4_C384887.html" H 2725 7375 50  0001 C CNN
	1    2725 7375
	-1   0    0    1   
$EndComp
Wire Wire Line
	5975 5875 5675 5875
Wire Wire Line
	6275 4475 6300 4475
Wire Wire Line
	6300 4475 6300 4625
Wire Wire Line
	6275 4275 6300 4275
Wire Wire Line
	6300 4275 6300 4475
Connection ~ 6300 4475
Wire Wire Line
	5625 4175 5650 4175
Connection ~ 5650 4175
Wire Wire Line
	5325 4175 5300 4175
Wire Wire Line
	5275 3975 5275 3900
Wire Wire Line
	5275 3900 6800 3900
Wire Wire Line
	4650 4075 4650 4625
Wire Wire Line
	4650 4625 4975 4625
Connection ~ 4650 4075
Wire Wire Line
	4650 4075 4675 4075
Wire Wire Line
	5275 4625 5300 4625
Connection ~ 5300 4175
Wire Wire Line
	5300 4175 5275 4175
Wire Wire Line
	5300 4175 5300 4625
Wire Wire Line
	5325 4625 5300 4625
Connection ~ 5300 4625
Wire Wire Line
	5525 7250 5525 7425
Connection ~ 5525 7425
Wire Wire Line
	5525 7425 5275 7425
$Comp
L power:+5V #PWR0123
U 1 1 5FE0CA04
P 5525 6875
F 0 "#PWR0123" H 5525 6725 50  0001 C CNN
F 1 "+5V" V 5450 6900 50  0000 C CNN
F 2 "" H 5525 6875 50  0000 C CNN
F 3 "" H 5525 6875 50  0000 C CNN
	1    5525 6875
	1    0    0    -1  
$EndComp
Wire Wire Line
	5525 6875 5525 6900
Text GLabel 4975 8400 0    60   Input ~ 0
GAIN_SW
Wire Wire Line
	4975 8400 5350 8400
Text GLabel 6625 7125 2    60   Input ~ 0
GAIN_SW
Wire Wire Line
	5700 7125 6625 7125
Text GLabel 6625 5975 2    60   Input ~ 0
GAIN_SW
Wire Wire Line
	6625 5975 5675 5975
Connection ~ 7350 6175
Wire Wire Line
	7350 3300 7350 3750
Text GLabel 4975 4725 0    60   Input ~ 0
GAIN_SW
Wire Wire Line
	4975 4725 5325 4725
Connection ~ 7350 5000
Connection ~ 7350 3750
Wire Wire Line
	7350 3750 7350 5000
Wire Wire Line
	7350 2825 7350 3200
Wire Wire Line
	2425 7425 2325 7425
Wire Wire Line
	2325 6700 2325 7425
Wire Wire Line
	1600 6800 2400 6800
Wire Wire Line
	2425 7350 2400 7350
Wire Wire Line
	2400 7350 2400 6800
Connection ~ 2400 6800
Wire Wire Line
	2400 6800 3050 6800
Wire Wire Line
	2525 7175 3050 7175
Wire Wire Line
	3050 7175 3050 7350
Wire Wire Line
	3050 7350 3025 7350
Wire Wire Line
	2525 6900 2525 7175
Wire Wire Line
	2625 7100 3125 7100
Wire Wire Line
	3125 7100 3125 7425
Wire Wire Line
	3125 7425 3025 7425
Wire Wire Line
	2625 7000 2625 7100
$Comp
L power:+3.3V #PWR0124
U 1 1 5DBFD368
P 2800 7700
F 0 "#PWR0124" H 2800 7550 50  0001 C CNN
F 1 "+3.3V" V 2800 7950 50  0000 C CNN
F 2 "" H 2800 7700 50  0000 C CNN
F 3 "" H 2800 7700 50  0000 C CNN
	1    2800 7700
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0125
U 1 1 5DBFD550
P 2650 7700
F 0 "#PWR0125" H 2650 7450 50  0001 C CNN
F 1 "GND" H 2650 7550 50  0000 C CNN
F 2 "" H 2650 7700 50  0000 C CNN
F 3 "" H 2650 7700 50  0000 C CNN
	1    2650 7700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 7700 2650 7650
Wire Wire Line
	2800 7650 2800 7700
Text GLabel 1650 4200 2    60   Input ~ 0
GAIN_SW
Wire Wire Line
	1650 4200 1600 4200
Connection ~ 4825 1150
$Comp
L 3Phase:SY8113-3Phase-rescue-3Phase-rescue U10
U 1 1 5BDCEBDA
P 4525 1250
F 0 "U10" H 4375 1475 50  0000 R CNN
F 1 "SY8201ABC" H 4250 1250 50  0001 L CNN
F 2 "Footprints:TSOT-23-6" H 4525 675 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/DC-DC-Converters_Silergy_SY8201ABC_SY8201ABC_C108052.html" H 4525 1250 50  0001 C CNN
	1    4525 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4825 1250 4850 1250
Wire Wire Line
	4850 1300 4850 1250
Connection ~ 4850 1250
Wire Wire Line
	4850 1250 5125 1250
Wire Wire Line
	5300 1250 5300 1275
Wire Wire Line
	5300 1250 5450 1250
$Comp
L 3Phase:R-3Phase-rescue-3Phase-rescue R59
U 1 1 5E2C54F1
P 5125 1425
F 0 "R59" V 5205 1425 50  0000 C CNN
F 1 "150k" V 5125 1425 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5055 1425 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_RC0603JR-07270KL_C137642.html" H 5125 1425 50  0001 C CNN
	1    5125 1425
	-1   0    0    1   
$EndComp
Wire Wire Line
	5125 1250 5125 1275
Wire Wire Line
	5125 1575 5125 1600
Wire Wire Line
	5125 1600 5300 1600
Wire Wire Line
	5300 1600 5300 1575
Wire Wire Line
	5125 1600 5125 1775
Wire Wire Line
	4825 1775 5125 1775
Connection ~ 5125 1600
$EndSCHEMATC
