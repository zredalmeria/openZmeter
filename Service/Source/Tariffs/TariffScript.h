// This file is part of openZmeter.
//
// openZmeter is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// openZmeter is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with openZmeter.  If not, see <https://www.gnu.org/licenses/>
// ---------------------------------------------------------------------------------------------------------------------
#pragma once
#include <set>
#include <list>
#include <string>
#include "muParser/muParser.h"
// ---------------------------------------------------------------------------------------------------------------------
using namespace std;
using namespace mu;
// ---------------------------------------------------------------------------------------------------------------------
class TariffScript final {
  private :
    Parser        pParser;
    list<float>   pDefVariables;
    string        pExpression;
    bool          pInitialized;
  public :
    static void CheckConfig(const string &script, set<string> &variables);
  private :
    static float *VariableFactory(const char_type *name, void *params);
  public :
    TariffScript(const string &expr);
    ~TariffScript();
    void  Var(const string &name, float *val);
    float Var(const string &name);
    float Eval();
};
// ---------------------------------------------------------------------------------------------------------------------